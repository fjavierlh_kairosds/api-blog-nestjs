import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { AuthorsRepositoryMongo } from '../src/authors/infrastructure/author.repository.mongo';
import { UsersRepositoryPostgres } from '../src/users/infrastructure/user.repository.pg';

describe('UsersController (e2e)', () => {
  let app: INestApplication;
  let usersRepository: UsersRepositoryPostgres;
  let authorsRepository: AuthorsRepositoryMongo;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    usersRepository = moduleFixture.get<UsersRepositoryPostgres>(
      UsersRepositoryPostgres,
    );
    authorsRepository = moduleFixture.get<AuthorsRepositoryMongo>(
      AuthorsRepositoryMongo,
    );

    await authorsRepository.deleteAll();
    await usersRepository.deleteAll();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/sign-up (POST): should return status code 201 if sign up data is valid', () => {
    return request(app.getHttpServer())
      .post('/users/sign-up')
      .type('application/json')
      .send({
        email: 'mail@mail.com',
        password: '1234',
        name: 'Test User',
        nickname: 'testuser',
      })
      .expect(201);
  });

  it('/sign-up (POST):should return status code 400 if sign up data is invalid', async () => {
    return request(app.getHttpServer())
      .post('/users/sign-up')
      .type('application/json')
      .send({
        invalid: 'Im a bad request',
      })
      .expect(400);
  });

  it('/sign-in (POST): should return token if sign in data is valid', async () => {
    await request(app.getHttpServer())
      .post('/users/sign-up')
      .type('application/json')
      .send({
        email: 'mail@mail.com',
        password: '1234',
        name: 'Test User',
        nickname: 'testuser',
      });

    const res = await request(app.getHttpServer())
      .post('/users/sign-in')
      .type('application/json')
      .send({
        email: 'mail@mail.com',
        password: '1234',
      })
      .expect(201);

    expect(res.body).toHaveProperty('token');
  });

  it('/sign-in (POST): should return token if sign in data is valid', async () => {
    const res = await request(app.getHttpServer())
      .post('/users/sign-in')
      .type('application/json')
      .send({
        email: 'mail@mail.com',
        password: '1234',
      })
      .expect(401);

    expect(res.body).not.toHaveProperty('token');
  });

  afterEach(async () => {
    await authorsRepository.deleteAll();
    await usersRepository.deleteAll();
    await app.close();
  });
});
