import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { IdVO } from '../src/common/domain/vo/id.vo';
import { PostRepositoryMongo } from '../src/posts/infrastructure/post.repository.mongo';
import { User, UserType } from '../src/users/domain/user.entity';
import { UserService } from '../src/users/domain/user.service';
import { EmailVO } from '../src/users/domain/vo/email.vo';
import { PasswordVO } from '../src/users/domain/vo/password.vo';
import { Role, RoleVO } from '../src/users/domain/vo/role.vo';
import { UsersRepositoryPostgres } from '../src/users/infrastructure/user.repository.pg';

describe('PostController (e2e)', () => {
  let app: INestApplication;
  let postRepository: PostRepositoryMongo;
  let usersRepository: UsersRepositoryPostgres;

  let tokenAdmin: string;
  let tokenPublisherOne: string;
  let tokenPublisherTwo: string;

  const emailAdmin = 'admin@mail.com';
  const passwordAdmin = 'password0';

  const emailPublisherOne = 'publisher1@mail.com';
  const passworPublisherOne = 'password1';

  const emailPublisherTwo = 'publisher2@mail.com';
  const passworPublisherTwo = 'password2';

  const adminData: UserType = {
    id: IdVO.create(),
    email: EmailVO.create(emailAdmin),
    password: PasswordVO.create(passwordAdmin),
    role: RoleVO.create(Role.ADMIN),
  };
  const publisherOneData: UserType = {
    id: IdVO.create(),
    email: EmailVO.create(emailPublisherOne),
    password: PasswordVO.create(passworPublisherOne),
    role: RoleVO.create(Role.PUBLISHER),
  };
  const publisherTwoData: UserType = {
    id: IdVO.create(),
    email: EmailVO.create(emailPublisherTwo),
    password: PasswordVO.create(passworPublisherTwo),
    role: RoleVO.create(Role.PUBLISHER),
  };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    postRepository =
      moduleFixture.get<PostRepositoryMongo>(PostRepositoryMongo);
    await postRepository.deleteAll();

    usersRepository = moduleFixture.get<UsersRepositoryPostgres>(
      UsersRepositoryPostgres,
    );
    await usersRepository.deleteAll();

    app = moduleFixture.createNestApplication();
    await app.init();

    const userService = moduleFixture.get<UserService>(UserService);

    await userService.persist(new User(adminData));
    await userService.persist(new User(publisherOneData));
    await userService.persist(new User(publisherTwoData));

    tokenPublisherOne = await request(app.getHttpServer())
      .post('/users/sign-in')
      .type('application/json')
      .send({
        email: emailPublisherOne,
        password: passworPublisherOne,
      })
      .then((result) => result.body.token);

    tokenPublisherTwo = await request(app.getHttpServer())
      .post('/users/sign-in')
      .type('application/json')
      .send({
        email: emailPublisherTwo,
        password: passworPublisherTwo,
      })
      .then((result) => result.body.token);
  });

  it('/posts (POST): shoud', async () => {
    tokenAdmin = await request(app.getHttpServer())
      .post('/users/sign-in')
      .type('application/json')
      .send({
        email: emailAdmin,
        password: passwordAdmin,
      })
      .then((result) => result.body.token);

    const response = await request(app.getHttpServer())
      .post('/posts')
      .type('application/json')
      .set('Authorization', `Bearer ${tokenAdmin}`)
      .send({
        author: 'Test Author',
        nickname: 'testauthor',
        title: 'My post title',
        content: '0123456789'.repeat(5),
      })
      .expect(201);

    expect(response.body).toHaveProperty('statusCode');
    expect(response.body).toHaveProperty('message');
    expect(response.body).toHaveProperty('newPost');
    expect(response.body.newPost).toHaveProperty('id');
    expect(response.body.newPost).toHaveProperty('author');
    expect(response.body.newPost).toHaveProperty('nickname');
    expect(response.body.newPost).toHaveProperty('title');
    expect(response.body.newPost).toHaveProperty('content');
    expect(response.body.newPost).toHaveProperty('comments');
  });

  afterEach(async () => {
    await postRepository.deleteAll();
    await usersRepository.deleteAll();
    await app.close();
  });
});
