import { Injectable } from '@nestjs/common';
import { ExceptionWithCode } from '../../common/domain/custom-errors/exception-with-code';
import { IdVO } from '../../common/domain/vo/id.vo';
import { AuthorsRepositoryMongo } from '../infrastructure/author.repository.mongo';
import { Author } from './author.entity';

@Injectable()
export class AuthorsService {
  constructor(private authorsRepository: AuthorsRepositoryMongo) {}

  async persist(author: Author): Promise<void> {
    await this.authorsRepository.createAuthor(author);
  }

  async findAuthorByID(authorID: IdVO): Promise<Author> {
    await this.checkIfAuthorExists(authorID);
    return this.authorsRepository.findAuthorByID(authorID);
  }

  private async checkIfAuthorExists(authorID: IdVO): Promise<void> {
    const exists = await this.authorsRepository.checkIfAuthorExists(authorID);
    if (!exists)
      throw new ExceptionWithCode(
        404,
        `Author with ID ${authorID.value} not found.`,
      );
  }
}
