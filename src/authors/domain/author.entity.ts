import { IdVO } from '../../common/domain/vo/id.vo';
import { AuthorNameVO } from './vo/author-name.vo';
import { AuthorNicknameVO } from './vo/author-nickname.vo';

export type AuthorType = {
  id: IdVO;
  name: AuthorNameVO;
  nickname: AuthorNicknameVO;
};

export class Author {
  constructor(private author: AuthorType) {}

  get id(): IdVO {
    return this.author.id;
  }

  get name(): AuthorNameVO {
    return this.author.name;
  }

  get nickname(): AuthorNicknameVO {
    return this.author.nickname;
  }
}
