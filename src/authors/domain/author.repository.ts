import { IdVO } from '../../common/domain/vo/id.vo';
import { Author } from './author.entity';

export interface AuthorsRepository {
  createAuthor(author: Author): Promise<void>;

  findAuthorByID(authorID: IdVO): Promise<Author>;

  checkIfAuthorExists(authorID: IdVO): Promise<boolean>;

  deleteAll(): Promise<void>;
}
