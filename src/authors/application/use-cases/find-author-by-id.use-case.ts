import { Injectable } from '@nestjs/common';
import { IdRequest } from '../../../common/application/types/id.request';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { Author } from '../../domain/author.entity';
import { AuthorsService } from '../../domain/authors.service';
import { AuthorResponse } from './types/author.response';

@Injectable()
export class FindAuthorByIDUseCase {
  constructor(private authorsService: AuthorsService) {}

  async execute(authorID: IdRequest): Promise<AuthorResponse> {
    const author: Author = await this.authorsService.findAuthorByID(
      IdVO.createWithUUID(authorID),
    );

    return {
      id: author.id.value,
      name: author.name.value,
      nickname: author.nickname.value,
    };
  }
}
