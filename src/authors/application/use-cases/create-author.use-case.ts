import { Injectable } from '@nestjs/common';
import { IdRequest } from '../../../common/application/types/id.request';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { Author, AuthorType } from '../../domain/author.entity';
import { AuthorsService } from '../../domain/authors.service';
import { AuthorNameVO } from '../../domain/vo/author-name.vo';
import { AuthorNicknameVO } from '../../domain/vo/author-nickname.vo';
import { AuthorRequest } from './types/author.request';

@Injectable()
export class CreateAuthorUseCase {
  constructor(private authorsService: AuthorsService) {}

  async execute(id: IdRequest, author: AuthorRequest): Promise<void> {
    const authorToType: AuthorType = {
      id: IdVO.createWithUUID(id),
      name: AuthorNameVO.create(author.name),
      nickname: AuthorNicknameVO.create(author.nickname),
    };

    await this.authorsService.persist(new Author(authorToType));
  }
}
