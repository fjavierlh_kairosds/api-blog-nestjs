jest.mock('../../infrastructure/author.repository.mongo', () => {
  return {
    AuthorsRepositoryMongo: jest.fn().mockImplementation(() => {
      return { createAuthor: jest.fn() };
    }),
  };
});

import { Test } from '@nestjs/testing';
import 'reflect-metadata';
import { AuthorsService } from '../../domain/authors.service';
import { AuthorsRepositoryMongo } from '../../infrastructure/author.repository.mongo';
import { CreateAuthorUseCase } from './create-author.use-case';
import { AuthorRequest } from './types/author.request';

describe('CreateAuthorUseCase test suite', () => {
  let authorRepository: AuthorsRepositoryMongo;
  let createAuthorUseCase: CreateAuthorUseCase;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [CreateAuthorUseCase, AuthorsService, AuthorsRepositoryMongo],
    }).compile();

    authorRepository = moduleRef.get<AuthorsRepositoryMongo>(
      AuthorsRepositoryMongo,
    );

    createAuthorUseCase =
      moduleRef.get<CreateAuthorUseCase>(CreateAuthorUseCase);
  });

  it('should create an author', async () => {
    const spyRepository = jest.spyOn(authorRepository, 'createAuthor');

    const validUUID = 'f2dd593e-af8e-4754-bed3-b42d2cfce636';
    const validAuthor: AuthorRequest = {
      name: 'My Test Author',
      nickname: 't-nickname',
    };

    await createAuthorUseCase.execute(validUUID, validAuthor);

    expect(spyRepository).toBeCalled();
  });
});
