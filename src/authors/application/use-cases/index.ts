import { CreateAuthorUseCase } from './create-author.use-case';
import { FindAuthorByIDUseCase } from './find-author-by-id.use-case';

export const AuhtorsUseCases = [CreateAuthorUseCase, FindAuthorByIDUseCase];
