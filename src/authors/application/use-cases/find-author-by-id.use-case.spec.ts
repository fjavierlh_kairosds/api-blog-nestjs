jest.mock('../../infrastructure/author.repository.mongo', () => {
  return {
    AuthorsRepositoryMongo: jest.fn().mockImplementation(() => {
      return {
        checkIfAuthorExists: jest.fn().mockImplementation(() => true),
        findAuthorByID: jest.fn().mockImplementation(() => {
          return new Author({
            id: IdVO.createWithUUID('54cc213e-478c-46b0-99bb-d6036567f25d'),
            name: AuthorNameVO.create('Test Author'),
            nickname: AuthorNicknameVO.create('testauthor'),
          });
        }),
      };
    }),
  };
});

import { Test } from '@nestjs/testing';
import 'reflect-metadata';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { Author } from '../../domain/author.entity';
import { AuthorsRepository } from '../../domain/author.repository';
import { AuthorsService } from '../../domain/authors.service';
import { AuthorNameVO } from '../../domain/vo/author-name.vo';
import { AuthorNicknameVO } from '../../domain/vo/author-nickname.vo';
import { AuthorsRepositoryMongo } from '../../infrastructure/author.repository.mongo';
import { FindAuthorByIDUseCase } from './find-author-by-id.use-case';

describe('FindAuthorByIDUseCase test suite', () => {
  let authorsRepository: AuthorsRepository;
  let findAuthorUseCase: FindAuthorByIDUseCase;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        FindAuthorByIDUseCase,
        AuthorsService,
        AuthorsRepositoryMongo,
      ],
    }).compile();

    authorsRepository = moduleRef.get<AuthorsRepositoryMongo>(
      AuthorsRepositoryMongo,
    );

    findAuthorUseCase = moduleRef.get<FindAuthorByIDUseCase>(
      FindAuthorByIDUseCase,
    );
  });
  it('should return an author if exists', async () => {
    const validAuthorID = '54cc213e-478c-46b0-99bb-d6036567f25d';
    const expectedAuthor = await findAuthorUseCase.execute(validAuthorID);
    console.log('expectedAuthor', expectedAuthor);
    expect(authorsRepository.checkIfAuthorExists).toBeCalled();
    expect(authorsRepository.findAuthorByID).toBeCalled();
    expect(expectedAuthor.id).toBe('54cc213e-478c-46b0-99bb-d6036567f25d');
    expect(expectedAuthor.name).toBe('Test Author');
    expect(expectedAuthor.nickname).toBe('testauthor');
  });
});
