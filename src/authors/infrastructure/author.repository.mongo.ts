import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { AnyObject, Model } from 'mongoose';
import { IdVO } from '../../common/domain/vo/id.vo';
import { Author } from '../domain/author.entity';
import { AuthorsRepository } from '../domain/author.repository';
import { AuthorNameVO } from '../domain/vo/author-name.vo';
import { AuthorNicknameVO } from '../domain/vo/author-nickname.vo';
import { AuthorDocument, AuthorMongo as AuthorModel } from './author.schema';

@Injectable()
export class AuthorsRepositoryMongo implements AuthorsRepository {
  constructor(
    @InjectModel(AuthorModel.name) private authorModel: Model<AuthorDocument>,
  ) {}

  async createAuthor(author: Author): Promise<void> {
    const authorData = {
      id: author.id.value,
      name: author.name.value,
      nickname: author.nickname.value,
    };

    const newAuthor = new this.authorModel(authorData);
    await newAuthor.save();
  }

  async findAuthorByID(authorID: IdVO): Promise<Author> {
    const author: AnyObject = await this.authorModel.findOne({
      id: authorID.value,
    });

    const authorType = {
      id: IdVO.createWithUUID(author.id),
      name: AuthorNameVO.create(author.name),
      nickname: AuthorNicknameVO.create(author.nickname),
    };

    return new Author(authorType);
  }

  async checkIfAuthorExists(authorID: IdVO): Promise<boolean> {
    return this.authorModel.exists({ id: authorID.value });
  }

  async deleteAll(): Promise<void> {
    await this.authorModel.deleteMany();
  }
}
