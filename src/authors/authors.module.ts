import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CreateAuthorUseCase } from './application/use-cases/create-author.use-case';
import { FindAuthorByIDUseCase } from './application/use-cases/find-author-by-id.use-case';
import { AuthorsService } from './domain/authors.service';
import { AuthorsRepositoryMongo } from './infrastructure/author.repository.mongo';
import { AuthorMongo, AuthorSchema } from './infrastructure/author.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: AuthorMongo.name, schema: AuthorSchema },
    ]),
  ],
  providers: [
    AuthorsService,
    AuthorsRepositoryMongo,
    CreateAuthorUseCase,
    FindAuthorByIDUseCase,
  ],
  exports: [CreateAuthorUseCase, FindAuthorByIDUseCase],
})
export class AuthorsModule {}
