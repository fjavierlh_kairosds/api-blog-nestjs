/* eslint-disable prettier/prettier */
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
  Res,
  UseGuards
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { Roles } from '../../common/infrastructure/roles.decorator';
import { RolesGuard } from '../../common/infrastructure/roles.guard';
import { Role } from '../../users/domain/vo/role.vo';
import { CreateOffensiveWordDTO } from '../application/dto/create-offensive-word.dto';
import { UpdateOffensiveWordDTO } from '../application/dto/update-offensive-word.dto';
import { CreateOffensiveWordUseCase } from '../application/use-cases/create-offensive-word.use-case';
import { DeleteOffensiveUseCase } from '../application/use-cases/delete-offensive-word.use-case';
import { FindAllOffensiveWordsUseCase } from '../application/use-cases/find-all-offensive-word.use-case';
import { FindOffensiveWordByIdUseCase } from '../application/use-cases/find-offensive-word-by-id.use-case';
import { UpdateOffensiveWordUseCase } from '../application/use-cases/update-offensive-word-by-id.use-case';

@Controller('offensive-words')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'), RolesGuard)
export class OffensiveWordController {
  constructor(
    private createOffensiveWordUseCase: CreateOffensiveWordUseCase,
    private findAllOffensiveWords: FindAllOffensiveWordsUseCase,
    private findOffensiveWordByIDUseCase: FindOffensiveWordByIdUseCase,
    private updateOffensiveWordUseCase: UpdateOffensiveWordUseCase,
    private deleteOffensiveWord: DeleteOffensiveUseCase,
  ) {}

  @Post()
  @Roles(Role.ADMIN)
  async create(
    @Req() req: Request,
    @Body() newOffensiveWord: CreateOffensiveWordDTO,
    @Res() res: Response,
  ) {
    try {
      //console.log(req)
      await this.createOffensiveWordUseCase.execute(newOffensiveWord);
      const { word, level } = newOffensiveWord;
      res.status(201).json({
        statusCode: 201,
        message: `offensive word '${word}' with level ${level} created`,
        newOffensiveWord,
      });
    } catch (error) {
      return res.status(400).json({ errors: [{ msg: error.message }] });
    }
  }

  @Get()
  @Roles(Role.ADMIN)
  async getAll(@Res() res: Response) {
    try {
      const allOffensiveWords = await this.findAllOffensiveWords.execute();
      res.status(200).json({
        statusCode: 200,
        message: `Showing ${allOffensiveWords.length} offensive words`,
        allOffensiveWords,
      });
    } catch (error) {
      return res.status(400).json({ errors: [{ msg: error.message }] });
    }
  }

  @Get(':id')
  @Roles(Role.ADMIN)
  async getById(@Param('id') id: string, @Res() res: Response) {
    try {
      const searchedOffensiveWord =
        await this.findOffensiveWordByIDUseCase.execute(id);
      res.status(200).json({
        statusCode: 200,
        message: `Offensive word with ID '${id}' found`,
        searchedOffensiveWord,
      });
    } catch (error) {
      return res.status(400).json({ errors: [{ msg: error.message }] });
    }
  }

  @Put(':id')
  @Roles(Role.ADMIN)
  async updateById(
    @Param('id') id: string,
    @Body() updatedOffensiveWord: UpdateOffensiveWordDTO,
    @Res() res: Response,
  ) {
    try {
      const updateResult = await this.updateOffensiveWordUseCase.execute(
        id,
        updatedOffensiveWord,
      );
      res.status(200).json({
        statusCode: 200,
        message: `Offensive word with ID '${id}' updated`,
        updateResult,
      });
    } catch (error) {
      return res.status(400).json({ errors: [{ msg: error.message }] });
    }
  }

  @Delete(':id')
  @Roles(Role.ADMIN)
  async deleteById(@Param('id') id: string, @Res() res: Response) {
    try {
      await this.deleteOffensiveWord.execute(id);
      res.status(200).json({
        statusCode: 200,
        message: `Offensive word with ID '${id}' deleted`,
      });
    } catch (error) {
      return res.status(400).json({ errors: [{ msg: error.message }] });
    }
  }
}
