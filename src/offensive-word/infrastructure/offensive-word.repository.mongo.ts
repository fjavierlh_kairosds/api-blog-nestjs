/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { AnyObject, Model } from 'mongoose';
import { IdVO } from '../../common/domain/vo/id.vo';
import {
  OffensiveWord,
  OffensiveWordType
} from '../domain/offensive-word.entity';
import { OffensiveWordRepository } from '../domain/offensive-word.repository';
import { LevelVO } from '../domain/vo/level.vo';
import { WordVO } from '../domain/vo/word.vo';
import { OffensiveWordMongo as OffensiveWordModel } from './offensive-word.schema';

@Injectable()
export class OffensiveWordRepositoryMongo implements OffensiveWordRepository {
  constructor(
    @InjectModel(OffensiveWordModel.name)
    private offensiveWordModel: Model<OffensiveWordModel>,
  ) {}

  async save(offensiveWord: OffensiveWord): Promise<void> {
    const newOffensiveWord = {
      id: offensiveWord.id.value,
      word: offensiveWord.word.value,
      level: offensiveWord.level.value,
    };

    const offensiveWordModel = new this.offensiveWordModel(newOffensiveWord);
    await offensiveWordModel.save();
  }

  
  async delete(idOffensiveWord: IdVO): Promise<void> {
    this.offensiveWordModel.deleteOne({ id: idOffensiveWord.value }).exec();
  }

  async showAll(): Promise<OffensiveWord[]> {
    const allOWData = await this.offensiveWordModel.find().exec();

    return allOWData.map((ow: AnyObject) => {
      const offensiveWordToModel: OffensiveWordType = {
        id: IdVO.createWithUUID(ow.id),
        word: WordVO.create(ow.word),
        level: LevelVO.create(ow.level),
      };

      return new OffensiveWord(offensiveWordToModel);
    });
  }


  async findById(idOffensiveWord: IdVO): Promise<OffensiveWord | null> {
    const searchedOffensiveWord: AnyObject = await this.offensiveWordModel.findOne({
      id: idOffensiveWord.value,
    }).exec();

    if (!searchedOffensiveWord) return null;

    const offensiveWordModel: OffensiveWordType = {
      id: IdVO.createWithUUID(searchedOffensiveWord.id),
      word: WordVO.create(searchedOffensiveWord.word),
      level: LevelVO.create(searchedOffensiveWord.level),
    };

    return new OffensiveWord(offensiveWordModel);
  }

  /**
   *
   * @param idOffensiveWord
   * @param offensiveWord
   * @returns
   */
  async update(
    idOffensiveWord: IdVO,
    offensiveWord: OffensiveWord,
  ): Promise<OffensiveWord> {

    const searchedOffensiveWord: AnyObject = await this.findById(
      idOffensiveWord,
    );

    const updatedOFType = {
      word: offensiveWord.word.value ?? searchedOffensiveWord.word.value,
      level: offensiveWord.level.value ?? searchedOffensiveWord.level.value
    };

    const updatedOF: AnyObject = await this.offensiveWordModel.findOneAndUpdate(
      { id: idOffensiveWord.value },
      updatedOFType,
    );

    const updatedOffensiveWordData: OffensiveWordType = {
      id: updatedOF.id,
      word: WordVO.create(updatedOFType.word) ?? updatedOF.word,
      level: LevelVO.create(updatedOFType.level) ?? updatedOF.level,
    };

    return new OffensiveWord(updatedOffensiveWordData);
  }

  async deleteAll(): Promise<void> {
    await this.offensiveWordModel.deleteMany();
  }
}
