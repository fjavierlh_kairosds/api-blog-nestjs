/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { RolesGuard } from '../common/infrastructure/roles.guard';
import { UserService } from '../users/domain/user.service';
import { UsersRepositoryPostgres } from '../users/infrastructure/user.repository.pg';
import { UsersModule } from '../users/users.module';
import { CreateOffensiveWordUseCase } from './application/use-cases/create-offensive-word.use-case';
import { DeleteOffensiveUseCase } from './application/use-cases/delete-offensive-word.use-case';
import { FindAllOffensiveWordsUseCase } from './application/use-cases/find-all-offensive-word.use-case';
import { FindOffensiveWordByIdUseCase } from './application/use-cases/find-offensive-word-by-id.use-case';
import { UpdateOffensiveWordUseCase } from './application/use-cases/update-offensive-word-by-id.use-case';
import { OffensiveWordService } from './domain/offensive-word.service';
import { OffensiveWordController } from './infrastructure/offensive-word.controller';
import { OffensiveWordRepositoryMongo } from './infrastructure/offensive-word.repository.mongo';
import {
  OffensiveWordMongo,
  OffensiveWordSchema
} from './infrastructure/offensive-word.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: OffensiveWordMongo.name, schema: OffensiveWordSchema },
    ]),
    UsersModule,
  ],
  providers: [
    OffensiveWordRepositoryMongo,
    OffensiveWordService,
    CreateOffensiveWordUseCase,
    FindAllOffensiveWordsUseCase,
    FindOffensiveWordByIdUseCase,
    UpdateOffensiveWordUseCase,
    DeleteOffensiveUseCase,
    RolesGuard,
    UserService,
    UsersRepositoryPostgres
  ],
  controllers: [OffensiveWordController],
  exports: [OffensiveWordService, OffensiveWordRepositoryMongo],
})
export class OffensiveWordModule {}
