import { IdVO } from '../../common/domain/vo/id.vo';
import { OffensiveWord } from './offensive-word.entity';

export interface OffensiveWordRepository {
  save(offensiveWord: OffensiveWord): Promise<void>;

  delete(idOffensiveWord: IdVO): Promise<void>;

  showAll(): Promise<OffensiveWord[]>;

  findById(idOffensiveWord: IdVO): Promise<OffensiveWord | null>;

  update(
    idOffensiveWord: IdVO,
    offensiveWord: OffensiveWord,
  ): Promise<OffensiveWord>;

  deleteAll(): Promise<void>;
}
