import { ApiProperty } from '@nestjs/swagger';

export class UpdateOffensiveWordDTO {
  @ApiProperty()
  readonly word?: string;

  @ApiProperty()
  readonly level?: number;

  constructor(word: string, level: number) {
    this.word = word;
    this.level = level;
  }
}
