import { Injectable } from '@nestjs/common';
import { IdRequest } from '../../../common/application/types/id.request';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { OffensiveWord } from '../../domain/offensive-word.entity';
import { OffensiveWordService } from '../../domain/offensive-word.service';
import { OffensiveWordResponse } from './types/offensive-word.response';

@Injectable()
export class FindOffensiveWordByIdUseCase {
  constructor(private offensiveWordService: OffensiveWordService) {}

  async execute(
    idOffensiveWord: IdRequest,
  ): Promise<OffensiveWordResponse | null> {
    const id = IdVO.createWithUUID(idOffensiveWord);
    const searchedOffensiveWord: OffensiveWord | null =
      await this.offensiveWordService.showById(id);

    if (!searchedOffensiveWord) return null;

    return {
      id: id.value,
      word: searchedOffensiveWord.word.value,
      level: searchedOffensiveWord.level.value,
    };
  }
}
