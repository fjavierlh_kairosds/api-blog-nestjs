jest.mock('../../infrastructure/offensive-word.repository.mongo', () => {
  return {
    OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
      return {
        save: jest.fn(),
      };
    }),
  };
});

import { Test } from '@nestjs/testing';
import 'reflect-metadata';
import { OffensiveWordService } from '../../domain/offensive-word.service';
import { OffensiveWordRepositoryMongo } from '../../infrastructure/offensive-word.repository.mongo';
import { CreateOffensiveWordUseCase } from './create-offensive-word.use-case';
import { OffensiveWordRequest } from './types/offensive-word.request';

describe('CreateOffensiveWordUseCase', () => {
  let offesniveWordsRepository: OffensiveWordRepositoryMongo;
  let createOffensiveWordUseCase: CreateOffensiveWordUseCase;
  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        CreateOffensiveWordUseCase,
        OffensiveWordRepositoryMongo,
        OffensiveWordService,
      ],
    }).compile();

    offesniveWordsRepository = moduleRef.get<OffensiveWordRepositoryMongo>(
      OffensiveWordRepositoryMongo,
    );

    createOffensiveWordUseCase = moduleRef.get<CreateOffensiveWordUseCase>(
      CreateOffensiveWordUseCase,
    );
  });
  it('should create an offensive word and persit it', async () => {
    const offensiveWordRequest: OffensiveWordRequest = {
      word: 'Test',
      level: 1,
    };

    await createOffensiveWordUseCase.execute(offensiveWordRequest);
    expect(offesniveWordsRepository.save).toBeCalled();
  });
});
