jest.mock('../../infrastructure/offensive-word.repository.mongo', () => {
  return {
    OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
      return {
        update: jest.fn().mockImplementation(
          () =>
            new OffensiveWord({
              id: IdVO.createWithUUID('f2dd593e-af8e-4754-bed3-b42d2cfce636'),
              word: WordVO.create('Tested'),
              level: LevelVO.create(1),
            }),
        ),
        findById: jest.fn().mockImplementation(
          () =>
            new OffensiveWord({
              id: IdVO.createWithUUID('f2dd593e-af8e-4754-bed3-b42d2cfce636'),
              word: WordVO.create('Test'),
              level: LevelVO.create(1),
            }),
        ),
      };
    }),
  };
});

import { Test } from '@nestjs/testing';
import 'reflect-metadata';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { OffensiveWord } from '../../domain/offensive-word.entity';
import { OffensiveWordService } from '../../domain/offensive-word.service';
import { LevelVO } from '../../domain/vo/level.vo';
import { WordVO } from '../../domain/vo/word.vo';
import { OffensiveWordRepositoryMongo } from '../../infrastructure/offensive-word.repository.mongo';
import { UpdateOffensiveWordUseCase } from './update-offensive-word-by-id.use-case';

describe('UpdateOffensiveWordById Test Suite', () => {
  let offesniveWordsRepository: OffensiveWordRepositoryMongo;
  let updateOffensiveWordByIdUseCase: UpdateOffensiveWordUseCase;
  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        UpdateOffensiveWordUseCase,
        OffensiveWordRepositoryMongo,
        OffensiveWordService,
      ],
    }).compile();

    offesniveWordsRepository = moduleRef.get<OffensiveWordRepositoryMongo>(
      OffensiveWordRepositoryMongo,
    );

    updateOffensiveWordByIdUseCase = moduleRef.get<UpdateOffensiveWordUseCase>(
      UpdateOffensiveWordUseCase,
    );
  });
  it('should return a offensive word by id', async () => {
    const testId = 'f2dd593e-af8e-4754-bed3-b42d2cfce636';
    const updatedOF = { word: 'Tested', level: 1 };

    const offensiveWord = await updateOffensiveWordByIdUseCase.execute(
      testId,
      updatedOF,
    );

    expect(offensiveWord.id).toEqual(testId);
    expect(offensiveWord.word).toEqual('Tested');
    expect(offensiveWord.level).toEqual(1);
    expect(offesniveWordsRepository.update).toHaveBeenCalled();
  });
});
