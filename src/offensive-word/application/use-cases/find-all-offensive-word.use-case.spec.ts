jest.mock('../../infrastructure/offensive-word.repository.mongo', () => {
  return {
    OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
      return {
        showAll: jest.fn().mockImplementation(() => [
          new OffensiveWord({
            id: IdVO.create(),
            word: WordVO.create('Test1'),
            level: LevelVO.create(1),
          }),
          new OffensiveWord({
            id: IdVO.create(),
            word: WordVO.create('Test2'),
            level: LevelVO.create(2),
          }),
          new OffensiveWord({
            id: IdVO.create(),
            word: WordVO.create('Test3'),
            level: LevelVO.create(3),
          }),
          new OffensiveWord({
            id: IdVO.create(),
            word: WordVO.create('Test4'),
            level: LevelVO.create(4),
          }),
          new OffensiveWord({
            id: IdVO.create(),
            word: WordVO.create('Test5'),
            level: LevelVO.create(5),
          }),
        ]),
      };
    }),
  };
});

import { Test } from '@nestjs/testing';
import 'reflect-metadata';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { OffensiveWord } from '../../domain/offensive-word.entity';
import { OffensiveWordService } from '../../domain/offensive-word.service';
import { LevelVO } from '../../domain/vo/level.vo';
import { WordVO } from '../../domain/vo/word.vo';
import { OffensiveWordRepositoryMongo } from '../../infrastructure/offensive-word.repository.mongo';
import { FindAllOffensiveWordsUseCase } from './find-all-offensive-word.use-case';

describe('FindAllOffensiveWordsUseCase Test Suite', () => {
  let offesniveWordsRepository: OffensiveWordRepositoryMongo;
  let findAllOffensiveWordsUseCase: FindAllOffensiveWordsUseCase;
  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        FindAllOffensiveWordsUseCase,
        OffensiveWordRepositoryMongo,
        OffensiveWordService,
      ],
    }).compile();

    offesniveWordsRepository = moduleRef.get<OffensiveWordRepositoryMongo>(
      OffensiveWordRepositoryMongo,
    );

    findAllOffensiveWordsUseCase = moduleRef.get<FindAllOffensiveWordsUseCase>(
      FindAllOffensiveWordsUseCase,
    );
  });
  it('should return all offensive words persisted in repository', async () => {
    const offensiveWords = await findAllOffensiveWordsUseCase.execute();

    expect(offensiveWords.length).toBe(5);
    expect(offensiveWords[4].level).toEqual(5);
    expect(offensiveWords[4].word).toEqual('Test5');
    expect(offesniveWordsRepository.showAll).toHaveBeenCalled();
  });
});
