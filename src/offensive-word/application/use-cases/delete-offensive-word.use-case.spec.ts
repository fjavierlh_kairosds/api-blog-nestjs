jest.mock('../../infrastructure/offensive-word.repository.mongo', () => {
  return {
    OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
      return {
        delete: jest.fn(),
        findById: jest.fn().mockImplementation(
          () =>
            new OffensiveWord({
              id: IdVO.createWithUUID('cf1e0d44-7b6a-4a0e-843c-cbc0e0b4a668'),
              word: WordVO.create('Test1'),
              level: LevelVO.create(1),
            }),
        ),
      };
    }),
  };
});

import { Test } from '@nestjs/testing';
import 'reflect-metadata';
import { IdRequest } from '../../../common/application/types/id.request';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { OffensiveWord } from '../../domain/offensive-word.entity';
import { OffensiveWordService } from '../../domain/offensive-word.service';
import { LevelVO } from '../../domain/vo/level.vo';
import { WordVO } from '../../domain/vo/word.vo';
import { OffensiveWordRepositoryMongo } from '../../infrastructure/offensive-word.repository.mongo';
import { DeleteOffensiveUseCase } from './delete-offensive-word.use-case';

describe('DeleteOffensiveUseCase Test Suite', () => {
  let offesniveWordsRepository: OffensiveWordRepositoryMongo;
  let deleteOffensiveWordUseCase: DeleteOffensiveUseCase;
  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        DeleteOffensiveUseCase,
        OffensiveWordRepositoryMongo,
        OffensiveWordService,
      ],
    }).compile();

    offesniveWordsRepository = moduleRef.get<OffensiveWordRepositoryMongo>(
      OffensiveWordRepositoryMongo,
    );

    deleteOffensiveWordUseCase = moduleRef.get<DeleteOffensiveUseCase>(
      DeleteOffensiveUseCase,
    );
  });
  it('should delete an offensive word', async () => {
    const idOW: IdRequest = 'cf1e0d44-7b6a-4a0e-843c-cbc0e0b4a668';

    await deleteOffensiveWordUseCase.execute(idOW);
    expect(offesniveWordsRepository.delete).toHaveBeenCalled();
  });
});
