jest.mock('../../infrastructure/offensive-word.repository.mongo', () => {
  return {
    OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
      return {
        findById: jest.fn().mockImplementation(
          () =>
            new OffensiveWord({
              id: IdVO.createWithUUID('f2dd593e-af8e-4754-bed3-b42d2cfce636'),
              word: WordVO.create('Test1'),
              level: LevelVO.create(1),
            }),
        ),
      };
    }),
  };
});

import { Test } from '@nestjs/testing';
import 'reflect-metadata';
import { IdRequest } from '../../../common/application/types/id.request';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { OffensiveWord } from '../../domain/offensive-word.entity';
import { OffensiveWordService } from '../../domain/offensive-word.service';
import { LevelVO } from '../../domain/vo/level.vo';
import { WordVO } from '../../domain/vo/word.vo';
import { OffensiveWordRepositoryMongo } from '../../infrastructure/offensive-word.repository.mongo';
import { FindOffensiveWordByIdUseCase } from './find-offensive-word-by-id.use-case';

describe('FindOffensiveWordById Test Suite', () => {
  let offesniveWordsRepository: OffensiveWordRepositoryMongo;
  let findOffensiveWordByIdUseCase: FindOffensiveWordByIdUseCase;
  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        FindOffensiveWordByIdUseCase,
        OffensiveWordRepositoryMongo,
        OffensiveWordService,
      ],
    }).compile();

    offesniveWordsRepository = moduleRef.get<OffensiveWordRepositoryMongo>(
      OffensiveWordRepositoryMongo,
    );

    findOffensiveWordByIdUseCase = moduleRef.get<FindOffensiveWordByIdUseCase>(
      FindOffensiveWordByIdUseCase,
    );
  });
  it('should return a offensive word by id', async () => {
    const testId: IdRequest = 'f2dd593e-af8e-4754-bed3-b42d2cfce636';

    const offensiveWord = await findOffensiveWordByIdUseCase.execute(testId);
    console.log(offensiveWord);
    if (offensiveWord !== null) {
      expect(offensiveWord.id).toEqual(testId);
      expect(offensiveWord.word).toEqual('Test1');
      expect(offensiveWord.level).toEqual(1);
      expect(offesniveWordsRepository.findById).toHaveBeenCalled();
    }
  });
});
