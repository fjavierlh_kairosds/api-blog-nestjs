import { Injectable } from '@nestjs/common';
import { IdRequest } from '../../../common/application/types/id.request';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { OffensiveWordService } from '../../domain/offensive-word.service';

@Injectable()
export class DeleteOffensiveUseCase {
  constructor(private offensiveWordService: OffensiveWordService) {}

  async execute(idOffensiveWord: IdRequest): Promise<void> {
    const id = IdVO.createWithUUID(idOffensiveWord);
    await this.offensiveWordService.remove(id);
  }
}
