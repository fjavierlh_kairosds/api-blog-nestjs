/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { IdRequest } from '../../../common/application/types/id.request';
import { IdVO } from '../../../common/domain/vo/id.vo';
import {
  OffensiveWord, OffensiveWordType
} from '../../domain/offensive-word.entity';
import { OffensiveWordService } from '../../domain/offensive-word.service';
import { LevelVO } from '../../domain/vo/level.vo';
import { WordVO } from '../../domain/vo/word.vo';
import { UpdateOffensiveWordDTO } from '../dto/update-offensive-word.dto';
import { OffensiveWordResponse } from './types/offensive-word.response';

@Injectable()
export class UpdateOffensiveWordUseCase {
  constructor(private offensiveWordService: OffensiveWordService) {}

  async execute(
    idOffensiveWord: IdRequest,
    updatedOffensiveWord: UpdateOffensiveWordDTO
	,
  ): Promise<OffensiveWordResponse> {
    const updatedOffensiveWordData: OffensiveWordType = {
      id: IdVO.createWithUUID(idOffensiveWord),
      word: WordVO.create(updatedOffensiveWord.word),
      level: LevelVO.create(updatedOffensiveWord.level),
    };

    const updatedOffensiveWordType: OffensiveWord =
      await this.offensiveWordService.updateById(
        updatedOffensiveWordData.id,
        updatedOffensiveWordData,
      );

    return {
      id: updatedOffensiveWordType.id.value,
      word: updatedOffensiveWordType.word.value,
      level: updatedOffensiveWordType.level.value,
    };
  }
}
