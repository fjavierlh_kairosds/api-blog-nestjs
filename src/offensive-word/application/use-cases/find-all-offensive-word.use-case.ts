import { Injectable } from '@nestjs/common';
import { OffensiveWord } from '../../domain/offensive-word.entity';
import { OffensiveWordService } from '../../domain/offensive-word.service';
import { OffensiveWordResponse } from './types/offensive-word.response';

@Injectable()
export class FindAllOffensiveWordsUseCase {
  constructor(private offensiveWordService: OffensiveWordService) {}

  async execute(): Promise<OffensiveWordResponse[]> {
    const allOffensiveWords: OffensiveWord[] =
      await this.offensiveWordService.showAll();

    const allOffensiveWordsResponse: OffensiveWordResponse[] =
      allOffensiveWords.map((ow: OffensiveWord) => {
        return { id: ow.id.value, word: ow.word.value, level: ow.level.value };
      });

    return allOffensiveWordsResponse;
  }
}
