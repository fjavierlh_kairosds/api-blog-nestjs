import { Injectable } from '@nestjs/common';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { OffensiveWordType } from '../../domain/offensive-word.entity';
import { OffensiveWordService } from '../../domain/offensive-word.service';
import { LevelVO } from '../../domain/vo/level.vo';
import { WordVO } from '../../domain/vo/word.vo';
import { OffensiveWordRequest } from './types/offensive-word.request';
import { OffensiveWordResponse } from './types/offensive-word.response';

@Injectable()
export class CreateOffensiveWordUseCase {
  constructor(private offensiveWordService: OffensiveWordService) {}

  async execute(
    offensiveWordRequest: OffensiveWordRequest,
  ): Promise<OffensiveWordResponse> {
    const offensiveWordData: OffensiveWordType = {
      id: IdVO.create(),
      word: WordVO.create(offensiveWordRequest.word),
      level: LevelVO.create(offensiveWordRequest.level),
    };

    await this.offensiveWordService.persist(offensiveWordData);

    return {
      id: offensiveWordData.id.value,
      word: offensiveWordData.word.value,
      level: offensiveWordData.level.value,
    };
  }
}
