import { Module } from '@nestjs/common';
import { AuthorsModule } from './authors/authors.module';
import { EnvConfig } from './common/infrastructure/config/env.config';
import { MongooseConfig } from './common/infrastructure/config/mongoose.config';
import { SequelizeConfig } from './common/infrastructure/config/sequelize.config';
import { OffensiveWordModule } from './offensive-word/offensive-word.module';
import { PostsModule } from './posts/posts.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    EnvConfig,
    MongooseConfig,
    SequelizeConfig,
    AuthorsModule,
    UsersModule,
    PostsModule,
    OffensiveWordModule,
  ],
})
export class AppModule {}
