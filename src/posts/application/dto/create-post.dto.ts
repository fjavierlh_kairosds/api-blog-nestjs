import { ApiProperty } from '@nestjs/swagger';
export class CreatePostDTO {
  @ApiProperty()
  author: string;

  @ApiProperty()
  nickname: string;

  @ApiProperty()
  title: string;

  @ApiProperty()
  content: string;

  constructor(
    author: string,
    nickname: string,
    title: string,
    content: string,
  ) {
    this.author = author;
    this.nickname = nickname;
    this.title = title;
    this.content = content;
  }
}
