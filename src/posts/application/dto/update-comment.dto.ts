import { ApiProperty } from '@nestjs/swagger';

export class UpdateCommentDTO {
  @ApiProperty()
  readonly nickname: string;

  @ApiProperty()
  readonly content: string;

  constructor(nickname: string, content: string) {
    this.nickname = nickname;
    this.content = content;
  }
}
