import { Test } from '@nestjs/testing';
import { IdRequest } from '../../../common/application/types/id.request';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { OffensiveWord } from '../../../offensive-word/domain/offensive-word.entity';
import { OffensiveWordService } from '../../../offensive-word/domain/offensive-word.service';
import { LevelVO } from '../../../offensive-word/domain/vo/level.vo';
import { WordVO } from '../../../offensive-word/domain/vo/word.vo';
import { OffensiveWordRepositoryMongo } from '../../../offensive-word/infrastructure/offensive-word.repository.mongo';
import { PostService } from '../../domain/post.service';
import { PostRepositoryMongo } from '../../infrastructure/post.repository.mongo';
import { AddCommentToPostUseCase } from './add-comment-to-post.use-case';
import { CommentPostRequest } from './types/comment-post.request';

jest.mock('../../infrastructure/post.repository.mongo', () => {
  return {
    PostRepositoryMongo: jest.fn().mockImplementation(() => {
      return {
        saveCommentInPost: jest.fn(),
        checkIfPostExists: jest.fn().mockImplementation(() => true),
      };
    }),
  };
});

jest.mock(
  '../../../offensive-word/infrastructure/offensive-word.repository.mongo',
  () => {
    return {
      OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
        return {
          showAll: jest.fn().mockImplementation(() => [
            new OffensiveWord({
              id: IdVO.create(),
              word: WordVO.create('Test1'),
              level: LevelVO.create(1),
            }),
            new OffensiveWord({
              id: IdVO.create(),
              word: WordVO.create('Test2'),
              level: LevelVO.create(2),
            }),
            new OffensiveWord({
              id: IdVO.create(),
              word: WordVO.create('Test3'),
              level: LevelVO.create(3),
            }),
            new OffensiveWord({
              id: IdVO.create(),
              word: WordVO.create('Test4'),
              level: LevelVO.create(4),
            }),
            new OffensiveWord({
              id: IdVO.create(),
              word: WordVO.create('Test5'),
              level: LevelVO.create(5),
            }),
          ]),
        };
      }),
    };
  },
);

describe('AddCommentToPostUseCase', () => {
  let postsRepository: PostRepositoryMongo;
  let offensiveWordRepository: OffensiveWordRepositoryMongo;
  let addCommentUseCase: AddCommentToPostUseCase;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        AddCommentToPostUseCase,
        PostRepositoryMongo,
        PostService,
        OffensiveWordService,
        OffensiveWordRepositoryMongo,
      ],
    }).compile();

    postsRepository = moduleRef.get<PostRepositoryMongo>(PostRepositoryMongo);
    offensiveWordRepository = moduleRef.get<OffensiveWordRepositoryMongo>(
      OffensiveWordRepositoryMongo,
    );

    addCommentUseCase = moduleRef.get<AddCommentToPostUseCase>(
      AddCommentToPostUseCase,
    );
  });
  it('should add a comment to existent post', async () => {
    const validId: IdRequest = 'cf1e0d44-7b6a-4a0e-843c-cbc0e0b4a668';
    const comment: CommentPostRequest = {
      nickname: 'validNick',
      content: 'This is a valid comment',
    };
    await addCommentUseCase.execute(validId, comment);
    expect(postsRepository.saveCommentInPost).toHaveBeenCalled();
    expect(offensiveWordRepository.showAll).toHaveBeenCalled();
  });

  it('should throw an error if comment contains an offensive word', async () => {
    const validId: IdRequest = 'cf1e0d44-7b6a-4a0e-843c-cbc0e0b4a668';
    const comment: CommentPostRequest = {
      nickname: 'validNick',
      content:
        'This is a valid comment with offensive word: Test1, Test2, Test3, Test4, Test5',
    };

    await expect(addCommentUseCase.execute(validId, comment)).rejects.toThrow(
      'Found offensive words in comment: test1, test2, test3, test4, test5',
    );
  });
});
