import { Injectable } from '@nestjs/common';
import { Post } from '../../domain/entity/post.entity';
import { PostService } from '../../domain/post.service';
import { AllPostResponse } from './types/all-post.response';

@Injectable()
export class FindAllPostsUseCase {
  constructor(private postService: PostService) {}

  async execute(): Promise<AllPostResponse[]> {
    const allPosts: Post[] = await this.postService.findAllPosts();

    const allPostsToResponse: AllPostResponse[] = allPosts.map((post: Post) => {
      const { id, author, nickname, title, content } = post.toAnyType();
      return { id, author, nickname, title, content };
    });
    return allPostsToResponse;
  }
}
