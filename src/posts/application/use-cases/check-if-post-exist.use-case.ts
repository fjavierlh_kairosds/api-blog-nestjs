import { Injectable } from '@nestjs/common';
import { IdRequest } from '../../../common/application/types/id.request';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { PostService } from '../../domain/post.service';

@Injectable()
export class CheckIfPostIdExistUseCase {
  constructor(private postService: PostService) {}

  async execute(idPost: IdRequest): Promise<boolean> {
    return this.postService.checkIfPostExist(IdVO.createWithUUID(idPost));
  }
}
