import { Injectable } from '@nestjs/common';
import { IdRequest } from '../../../common/application/types/id.request';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { Post } from '../../domain/entity/post.entity';
import { PostService } from '../../domain/post.service';
import { SinglePostResponse } from './types/single-post.response';

@Injectable()
export class FindPostByIdUseCase {
  constructor(private postService: PostService) {}

  async execute(idPost: IdRequest): Promise<SinglePostResponse> {
    const expectedPost: Post = await this.postService.findPostById(
      IdVO.createWithUUID(idPost),
    );
    return expectedPost.toAnyType();
  }
}
