jest.mock('../../infrastructure/post.repository.mongo');

import { Test } from '@nestjs/testing';
import 'reflect-metadata';
import { PostService } from '../../domain/post.service';
import { PostRepositoryMongo } from '../../infrastructure/post.repository.mongo';
import { CreatePostUseCase } from './create-post.use-case';
import { PostRequest } from './types/post.request';

describe('CreatePostUseCase test suite', () => {
  let postsRepository: PostRepositoryMongo;
  let createPostUseCase: CreatePostUseCase;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [CreatePostUseCase, PostRepositoryMongo, PostService],
    }).compile();

    postsRepository = moduleRef.get<PostRepositoryMongo>(PostRepositoryMongo);

    createPostUseCase = moduleRef.get<CreatePostUseCase>(CreatePostUseCase);
  });
  it('should create a post', async () => {
    const postRequest: PostRequest = {
      author: 'Anónimo',
      nickname: 'anonymus',
      title: 'An anonymus post',
      content: 'loremimpsum'.repeat(6),
    };

    await createPostUseCase.execute(postRequest);
    expect(postsRepository.persistPost).toBeCalled();
  });
});
