import { Injectable } from '@nestjs/common';
import { AuthorNameVO } from '../../../authors/domain/vo/author-name.vo';
import { AuthorNicknameVO } from '../../../authors/domain/vo/author-nickname.vo';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { PostType } from '../../domain/entity/post.entity';
import { PostService } from '../../domain/post.service';
import { CommentsListVO } from '../../domain/vo/comments-list.vo';
import { PostContentVO } from '../../domain/vo/post-content.vo';
import { PostTitleVO } from '../../domain/vo/post-title.vo';
import { Post } from './../../domain/entity/post.entity';
import { PostRequest } from './types/post.request';
import { SinglePostResponse } from './types/single-post.response';

@Injectable()
export class CreatePostUseCase {
  constructor(private postService: PostService) {}

  async execute(newPost: PostRequest): Promise<SinglePostResponse> {
    const postToType: PostType = {
      id: IdVO.create(),
      author: AuthorNameVO.create(newPost.author),
      nickname: AuthorNicknameVO.create(newPost.nickname),
      title: PostTitleVO.create(newPost.title),
      content: PostContentVO.create(newPost.content),
      comments: CommentsListVO.create([]),
    };

    const newPostToEntity = new Post(postToType);
    await this.postService.savePost(newPostToEntity);

    return newPostToEntity.toAnyType();
  }
}
