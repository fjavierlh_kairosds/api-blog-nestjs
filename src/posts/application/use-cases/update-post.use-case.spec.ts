import { Test } from '@nestjs/testing';
import { AuthorNameVO } from '../../../authors/domain/vo/author-name.vo';
import { AuthorNicknameVO } from '../../../authors/domain/vo/author-nickname.vo';
import { IdRequest } from '../../../common/application/types/id.request';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { PostService } from '../../domain/post.service';
import { CommentsListVO } from '../../domain/vo/comments-list.vo';
import { PostContentVO } from '../../domain/vo/post-content.vo';
import { PostTitleVO } from '../../domain/vo/post-title.vo';
import { PostRepositoryMongo } from '../../infrastructure/post.repository.mongo';
import { Post } from './../../domain/entity/post.entity';
import { PostRequest } from './types/post.request';
import { SinglePostResponse } from './types/single-post.response';
import { UpdatePostUseCase } from './update-post.use-case';

jest.mock('../../infrastructure/post.repository.mongo', () => {
  return {
    PostRepositoryMongo: jest.fn().mockImplementation(() => {
      return {
        updatePost: jest.fn().mockImplementation(
          () =>
            new Post({
              id: IdVO.createWithUUID('cf1e0d44-7b6a-4a0e-843c-cbc0e0b4a668'),
              author: AuthorNameVO.create('Conocido'),
              nickname: AuthorNicknameVO.create('conocido'),
              title: PostTitleVO.create('An updated post'),
              content: PostContentVO.create('ipsumlorem'.repeat(6)),
              comments: CommentsListVO.create([]),
            }),
        ),
        checkIfPostExists: jest.fn().mockImplementation(() => true),
      };
    }),
  };
});

describe('UpdatePostUseCase test suite', () => {
  let postsRepository: PostRepositoryMongo;
  let updatePostUseCase: UpdatePostUseCase;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [UpdatePostUseCase, PostRepositoryMongo, PostService],
    }).compile();

    postsRepository = moduleRef.get<PostRepositoryMongo>(PostRepositoryMongo);

    updatePostUseCase = moduleRef.get<UpdatePostUseCase>(UpdatePostUseCase);
  });
  it('should update a post', async () => {
    const validId: IdRequest = 'cf1e0d44-7b6a-4a0e-843c-cbc0e0b4a668';
    const updatedPost: PostRequest = {
      author: 'Conocido',
      nickname: 'conocido',
      title: 'An updated post',
      content: 'ipsumlorem'.repeat(6),
    };

    const expectedUpdatedPost: SinglePostResponse | null =
      await updatePostUseCase.execute(validId, updatedPost);

    if (expectedUpdatedPost) {
      console.log('expectedUpdatedPost:', expectedUpdatedPost);
      expect(expectedUpdatedPost).not.toBeNull();
      expect(expectedUpdatedPost.id).toBe(validId);
      expect(expectedUpdatedPost.author).toBe(updatedPost.author);
      expect(expectedUpdatedPost.nickname).toBe(updatedPost.nickname);
      expect(expectedUpdatedPost.title).toBe(updatedPost.title);
      expect(expectedUpdatedPost.content).toBe(updatedPost.content);
      expect(postsRepository.updatePost).toHaveBeenCalled();
    }
  });
});
