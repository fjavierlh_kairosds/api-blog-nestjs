jest.mock('../../infrastructure/post.repository.mongo', () => {
  return {
    PostRepositoryMongo: jest.fn().mockImplementation(() => {
      return {
        deletePostByID: jest.fn(),
        checkIfPostExists: jest.fn().mockImplementation(() => true),
      };
    }),
  };
});

import { Test } from '@nestjs/testing';
import 'reflect-metadata';
import { PostService } from '../../domain/post.service';
import { PostRepositoryMongo } from '../../infrastructure/post.repository.mongo';
import { DeletePostUseCase } from './delete-post.use-case';
describe('DeletePostUseCase test suite', () => {
  let postsRepository: PostRepositoryMongo;
  let deletePostUseCase: DeletePostUseCase;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [DeletePostUseCase, PostRepositoryMongo, PostService],
    }).compile();

    postsRepository = moduleRef.get<PostRepositoryMongo>(PostRepositoryMongo);

    deletePostUseCase = moduleRef.get<DeletePostUseCase>(DeletePostUseCase);
  });
  it('should delete an post by ID', async () => {
    const validId = 'cf1e0d44-7b6a-4a0e-843c-cbc0e0b4a668';

    await deletePostUseCase.execute(validId);

    expect(postsRepository.deletePostByID).toHaveBeenCalled();
  });
});
