import { Test } from '@nestjs/testing';
import { AuthorNameVO } from '../../../authors/domain/vo/author-name.vo';
import { AuthorNicknameVO } from '../../../authors/domain/vo/author-nickname.vo';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { Post } from '../../domain/entity/post.entity';
import { PostService } from '../../domain/post.service';
import { CommentsListVO } from '../../domain/vo/comments-list.vo';
import { PostContentVO } from '../../domain/vo/post-content.vo';
import { PostTitleVO } from '../../domain/vo/post-title.vo';
import { PostRepositoryMongo } from '../../infrastructure/post.repository.mongo';
import { FindAllPostsUseCase } from './find-all-posts.use-case';

jest.mock('../../infrastructure/post.repository.mongo', () => {
  return {
    PostRepositoryMongo: jest.fn().mockImplementation(() => {
      return {
        getAllPosts: jest.fn().mockImplementation(() => [
          new Post({
            id: IdVO.createWithUUID('cf1e0d44-7b6a-4a0e-843c-cbc0e0b4a668'),
            author: AuthorNameVO.create('Anónimo'),
            nickname: AuthorNicknameVO.create('anonymus'),
            title: PostTitleVO.create('An anonymus post'),
            content: PostContentVO.create('loremimpsum'.repeat(6)),
            comments: CommentsListVO.create([]),
          }),
        ]),
      };
    }),
  };
});

describe('FindAllPostsUseCase test suite', () => {
  let postsRepository: PostRepositoryMongo;
  let findAllPostsUseCase: FindAllPostsUseCase;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [FindAllPostsUseCase, PostRepositoryMongo, PostService],
    }).compile();

    postsRepository = moduleRef.get<PostRepositoryMongo>(PostRepositoryMongo);

    findAllPostsUseCase =
      moduleRef.get<FindAllPostsUseCase>(FindAllPostsUseCase);
  });
  it('should return all posts', async () => {
    const allPosts = await findAllPostsUseCase.execute();
    expect(postsRepository.getAllPosts).toBeCalled();
    expect(allPosts.length).toBe(1);
    expect(allPosts[0].id).toBe('cf1e0d44-7b6a-4a0e-843c-cbc0e0b4a668');
    expect(allPosts[0].author).toBe('Anónimo');
    expect(allPosts[0].nickname).toBe('anonymus');
    expect(allPosts[0].title).toBe('An anonymus post');
    expect(allPosts[0].content).toBe('loremimpsum'.repeat(6));
    expect(Object.keys(allPosts[0]).includes('comments')).toBe(false);
  });
});
