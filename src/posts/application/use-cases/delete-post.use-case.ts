import { Injectable } from '@nestjs/common';
import { IdRequest } from '../../../common/application/types/id.request';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { PostService } from '../../domain/post.service';

@Injectable()
export class DeletePostUseCase {
  constructor(private postService: PostService) {}

  async execute(id: IdRequest): Promise<void> {
    await this.postService.removePostById(IdVO.createWithUUID(id));
  }
}
