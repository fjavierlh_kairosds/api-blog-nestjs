/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { IdRequest } from '../../../common/application/types/id.request';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { parseEnvVariableToNumber } from '../../../common/utils/parse-env-variable-to-number.util';
import { OffensiveWordService } from '../../../offensive-word/domain/offensive-word.service';
import { LevelVO } from '../../../offensive-word/domain/vo/level.vo';
import {
	CommentPost,
	CommentPostType
} from '../../domain/entity/comment-post.entity';
import { PostService } from '../../domain/post.service';
import { CommentContentVO } from '../../domain/vo/comment-content.vo';
import { CommentDateVO } from '../../domain/vo/comment-date.vo';
import { CommentNicknameVO } from '../../domain/vo/comment-nickname.vo';
import { CommentPostRequest } from './types/comment-post.request';
import { CommentPostResponse } from './types/comment-post.response';

@Injectable()
export class AddCommentToPostUseCase {
  constructor(
    private postService: PostService,
    private offensiveWordService: OffensiveWordService,
  ) {}

  async execute(
    postID: IdRequest,
    comment: CommentPostRequest,
  ): Promise<CommentPostResponse> {
    await this.offensiveWordService.chekWordsInComment(
      CommentContentVO.create(comment.content),
      LevelVO.create(
        parseEnvVariableToNumber(process.env.OFFENSIVE_WORD_LEVEL ?? '5'),
      ),
    );

    const commentToType: CommentPostType = {
      id: IdVO.create(),
      nickname: CommentNicknameVO.create(comment.nickname),
      content: CommentContentVO.create(comment.content),
      date: CommentDateVO.create(),
    };

    await this.postService.commentPost(
      IdVO.createWithUUID(postID),
      new CommentPost(commentToType),
    );

    return {
      id: commentToType.id.value,
      nickname: commentToType.nickname.value,
      content: commentToType.content.value,
      date: commentToType.content.value,
    };
  }
}
