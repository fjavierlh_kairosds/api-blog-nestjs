import { Injectable } from '@nestjs/common';
import { IdRequest } from '../../../common/application/types/id.request';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { parseEnvVariableToNumber } from '../../../common/utils/parse-env-variable-to-number.util';
import { OffensiveWordService } from '../../../offensive-word/domain/offensive-word.service';
import { LevelVO } from '../../../offensive-word/domain/vo/level.vo';
import { CommentPost } from '../../domain/entity/comment-post.entity';
import { PostService } from '../../domain/post.service';
import { CommentContentVO } from '../../domain/vo/comment-content.vo';
import { CommentDateVO } from '../../domain/vo/comment-date.vo';
import { CommentNicknameVO } from '../../domain/vo/comment-nickname.vo';
import { CommentPostRequest } from './types/comment-post.request';
import { CommentPostResponse } from './types/comment-post.response';

@Injectable()
export class UpdateCommentPostUseCase {
  constructor(
    private postService: PostService,
    private offensiveWordService: OffensiveWordService,
  ) {}

  async execute(
    postID: IdRequest,
    commentID: IdRequest,
    updatedComment: CommentPostRequest,
  ): Promise<CommentPostResponse> {
    await this.offensiveWordService.chekWordsInComment(
      CommentContentVO.create(updatedComment.content),
      LevelVO.create(
        parseEnvVariableToNumber(process.env.OFFENSIVE_WORD_LEVEL ?? '5'),
      ),
    );

    const commentToEntity = new CommentPost({
      id: IdVO.createWithUUID(commentID),
      nickname: CommentNicknameVO.create(updatedComment.nickname),
      content: CommentContentVO.create(updatedComment.content),
      date: CommentDateVO.create(),
    });

    await this.postService.updateCommentPost(
      IdVO.createWithUUID(postID),
      commentToEntity,
    );

    return {
      id: commentToEntity.id.value,
      nickname: commentToEntity.nickname.value,
      content: commentToEntity.content.value,
      date: commentToEntity.date.value,
    };
  }
}
