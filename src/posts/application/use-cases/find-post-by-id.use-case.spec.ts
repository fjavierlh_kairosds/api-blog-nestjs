jest.mock('../../infrastructure/post.repository.mongo', () => {
  return {
    PostRepositoryMongo: jest.fn().mockImplementation(() => {
      return {
        checkIfPostExists: jest.fn().mockImplementation(() => true),
        getPostByID: jest.fn().mockImplementation(
          () =>
            new Post({
              id: IdVO.createWithUUID('cf1e0d44-7b6a-4a0e-843c-cbc0e0b4a668'),
              author: AuthorNameVO.create('Anónimo'),
              nickname: AuthorNicknameVO.create('anonymus'),
              title: PostTitleVO.create('An anonymus post'),
              content: PostContentVO.create('loremimpsum'.repeat(6)),
              comments: CommentsListVO.create([
                new CommentPost({
                  id: IdVO.create(),
                  nickname: CommentNicknameVO.create('paquito85'),
                  content: CommentContentVO.create('Un comentario cualquiera'),
                  date: CommentDateVO.create(),
                }),
              ]),
            }),
        ),
      };
    }),
  };
});

import { Test } from '@nestjs/testing';
import { AuthorNameVO } from '../../../authors/domain/vo/author-name.vo';
import { AuthorNicknameVO } from '../../../authors/domain/vo/author-nickname.vo';
import { IdRequest } from '../../../common/application/types/id.request';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { CommentPost } from '../../domain/entity/comment-post.entity';
import { PostService } from '../../domain/post.service';
import { CommentContentVO } from '../../domain/vo/comment-content.vo';
import { CommentDateVO } from '../../domain/vo/comment-date.vo';
import { CommentNicknameVO } from '../../domain/vo/comment-nickname.vo';
import { CommentsListVO } from '../../domain/vo/comments-list.vo';
import { PostContentVO } from '../../domain/vo/post-content.vo';
import { PostTitleVO } from '../../domain/vo/post-title.vo';
import { PostRepositoryMongo } from '../../infrastructure/post.repository.mongo';
import { Post } from './../../domain/entity/post.entity';
import { FindPostByIdUseCase } from './find-post-by-id.use-case';
import { SinglePostResponse } from './types/single-post.response';

describe('FindPostByIdUseCase test suite', () => {
  let postsRepository: PostRepositoryMongo;
  let findPostByIdUseCase: FindPostByIdUseCase;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [FindPostByIdUseCase, PostRepositoryMongo, PostService],
    }).compile();

    postsRepository = moduleRef.get<PostRepositoryMongo>(PostRepositoryMongo);

    findPostByIdUseCase =
      moduleRef.get<FindPostByIdUseCase>(FindPostByIdUseCase);
  });
  it('should return searched post', async () => {
    const idRequest: IdRequest = 'cf1e0d44-7b6a-4a0e-843c-cbc0e0b4a668';

    const expectedPost: SinglePostResponse | null =
      await findPostByIdUseCase.execute(idRequest);
    console.log('expectedPost', expectedPost);

    if (expectedPost !== null) {
      expect(postsRepository.getPostByID).toBeCalled();
      expect(expectedPost.id).toBe(idRequest);
      expect(expectedPost.author).toBe('Anónimo');
      expect(expectedPost.nickname).toBe('anonymus');
      expect(expectedPost.title).toBe('An anonymus post');
      expect(expectedPost.content).toBe('loremimpsum'.repeat(6));
      expect(expectedPost.comments.length).toBe(1);
    }
  });
});
