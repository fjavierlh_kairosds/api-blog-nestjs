import { Injectable } from '@nestjs/common';
import { IdRequest } from '../../../common/application/types/id.request';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { PostService } from '../../domain/post.service';

@Injectable()
export class CheckIfCommentIdExistUseCase {
  constructor(private postService: PostService) {}

  async execute(postId: IdRequest, commentId: IdRequest): Promise<boolean> {
    return this.postService.checkIfCommentPostExist(
      IdVO.createWithUUID(postId),
      IdVO.createWithUUID(commentId),
    );
  }
}
