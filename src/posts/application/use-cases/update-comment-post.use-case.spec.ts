import { Test } from '@nestjs/testing';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { OffensiveWord } from '../../../offensive-word/domain/offensive-word.entity';
import { OffensiveWordService } from '../../../offensive-word/domain/offensive-word.service';
import { LevelVO } from '../../../offensive-word/domain/vo/level.vo';
import { WordVO } from '../../../offensive-word/domain/vo/word.vo';
import { OffensiveWordRepositoryMongo } from '../../../offensive-word/infrastructure/offensive-word.repository.mongo';
import { PostService } from '../../domain/post.service';
import { PostRepositoryMongo } from '../../infrastructure/post.repository.mongo';
import { CommentPostRequest } from './types/comment-post.request';
import { UpdateCommentPostUseCase } from './update-comment-post.use-case';

jest.mock('../../infrastructure/post.repository.mongo', () => {
  return {
    PostRepositoryMongo: jest.fn().mockImplementation(() => {
      return {
        updateCommentInPost: jest.fn(),
        checkIfPostExists: jest.fn().mockImplementation(() => true),
        checkIfCommentPostExists: jest.fn().mockImplementation(() => true),
      };
    }),
  };
});

jest.mock(
  '../../../offensive-word/infrastructure/offensive-word.repository.mongo',
  () => {
    return {
      OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
        return {
          showAll: jest.fn().mockImplementation(() => [
            new OffensiveWord({
              id: IdVO.create(),
              word: WordVO.create('Test1'),
              level: LevelVO.create(1),
            }),
            new OffensiveWord({
              id: IdVO.create(),
              word: WordVO.create('Test2'),
              level: LevelVO.create(2),
            }),
            new OffensiveWord({
              id: IdVO.create(),
              word: WordVO.create('Test3'),
              level: LevelVO.create(3),
            }),
            new OffensiveWord({
              id: IdVO.create(),
              word: WordVO.create('Test4'),
              level: LevelVO.create(4),
            }),
            new OffensiveWord({
              id: IdVO.create(),
              word: WordVO.create('Test5'),
              level: LevelVO.create(5),
            }),
          ]),
        };
      }),
    };
  },
);

describe('UpdateCommentPostUseCase test suite', () => {
  let postsRepository: PostRepositoryMongo;
  let offensiveWordRepository: OffensiveWordRepositoryMongo;
  let updateCommentUseCase: UpdateCommentPostUseCase;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        UpdateCommentPostUseCase,
        PostService,
        PostRepositoryMongo,
        OffensiveWordService,
        OffensiveWordRepositoryMongo,
      ],
    }).compile();

    postsRepository = moduleRef.get<PostRepositoryMongo>(PostRepositoryMongo);
    offensiveWordRepository = moduleRef.get<OffensiveWordRepositoryMongo>(
      OffensiveWordRepositoryMongo,
    );

    updateCommentUseCase = moduleRef.get<UpdateCommentPostUseCase>(
      UpdateCommentPostUseCase,
    );
  });
  it('should update an comment post', async () => {
    const validPostId = 'cf1e0d44-7b6a-4a0e-843c-cbc0e0b4a668';
    const validCommentId = 'f2dd593e-af8e-4754-bed3-b42d2cfce636';
    const updatedComment: CommentPostRequest = {
      nickname: 'paquito86',
      content: 'An updated comment',
    };

    await updateCommentUseCase.execute(
      validPostId,
      validCommentId,
      updatedComment,
    );
    expect(postsRepository.updateCommentInPost).toBeCalled();
    expect(offensiveWordRepository.showAll).toBeCalled();
  });

  it('should update an comment post', async () => {
    const validPostId = 'cf1e0d44-7b6a-4a0e-843c-cbc0e0b4a668';
    const validCommentId = 'f2dd593e-af8e-4754-bed3-b42d2cfce636';
    const updatedComment: CommentPostRequest = {
      nickname: 'paquito86',
      content:
        'An updated comment with offensive words:  Test1, Test2, Test3, Test4, Test5',
    };

    await expect(
      updateCommentUseCase.execute(validPostId, validCommentId, updatedComment),
    ).rejects.toThrow();
  });
});
