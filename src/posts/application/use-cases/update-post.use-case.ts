import { Injectable } from '@nestjs/common';
import { AuthorNameVO } from '../../../authors/domain/vo/author-name.vo';
import { AuthorNicknameVO } from '../../../authors/domain/vo/author-nickname.vo';
import { IdRequest } from '../../../common/application/types/id.request';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { Post, PostType } from '../../domain/entity/post.entity';
import { PostService } from '../../domain/post.service';
import { CommentsListVO } from '../../domain/vo/comments-list.vo';
import { PostContentVO } from '../../domain/vo/post-content.vo';
import { PostTitleVO } from '../../domain/vo/post-title.vo';
import { PostRequest } from './types/post.request';
import { SinglePostResponse } from './types/single-post.response';

@Injectable()
export class UpdatePostUseCase {
  constructor(private postService: PostService) {}

  async execute(
    receivedId: IdRequest,
    receivedPost: PostRequest,
  ): Promise<SinglePostResponse> {
    const idToVO = IdVO.createWithUUID(receivedId);

    const updatedPostToType: PostType = {
      id: idToVO,
      author: AuthorNameVO.create(receivedPost.author),
      nickname: AuthorNicknameVO.create(receivedPost.nickname),
      title: PostTitleVO.create(receivedPost.title),
      content: PostContentVO.create(receivedPost.content),
      comments: CommentsListVO.create([]),
    };

    const updatedPost: Post = await this.postService.updatePostByID(
      idToVO,
      new Post(updatedPostToType),
    );

    return updatedPost.toAnyType();
  }
}
