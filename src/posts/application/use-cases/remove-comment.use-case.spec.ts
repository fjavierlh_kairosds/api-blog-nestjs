jest.mock('../../infrastructure/post.repository.mongo', () => {
  return {
    PostRepositoryMongo: jest.fn().mockImplementation(() => {
      return {
        deleteCommentInPost: jest.fn(),
        checkIfPostExists: jest.fn().mockImplementation(() => true),
        checkIfCommentPostExists: jest.fn().mockImplementation(() => true),
      };
    }),
  };
});

import { Test } from '@nestjs/testing';
import { PostService } from '../../domain/post.service';
import { PostRepositoryMongo } from '../../infrastructure/post.repository.mongo';
import { DeleteCommentPostUseCase } from './remove-comment.use-case';

describe('DeleteCommentPostUseCase', () => {
  let postsRepository: PostRepositoryMongo;
  let deleteCommentUseCase: DeleteCommentPostUseCase;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [DeleteCommentPostUseCase, PostRepositoryMongo, PostService],
    }).compile();

    postsRepository = moduleRef.get<PostRepositoryMongo>(PostRepositoryMongo);

    deleteCommentUseCase = moduleRef.get<DeleteCommentPostUseCase>(
      DeleteCommentPostUseCase,
    );
  });
  it('should remove a existent comment', async () => {
    const postId = '82b486d3-e592-4f04-8056-91956dfbc7f3';
    const commentId = 'a792b2ee-7424-4f78-9a9c-3402676e80e0';

    const result = await deleteCommentUseCase.execute(postId, commentId);
    expect(postsRepository.deleteCommentInPost).toBeCalled();
    expect(result).not.toBeNull();
  });
});
