/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {
  OffensiveWordModule
} from '../offensive-word/offensive-word.module';
import { AddCommentToPostUseCase } from './application/use-cases/add-comment-to-post.use-case';
import { CheckIfCommentIdExistUseCase } from './application/use-cases/check-if-comment-exist.use-case';
import { CheckIfPostIdExistUseCase } from './application/use-cases/check-if-post-exist.use-case';
import { CreatePostUseCase } from './application/use-cases/create-post.use-case';
import { DeletePostUseCase } from './application/use-cases/delete-post.use-case';
import { FindAllPostsUseCase } from './application/use-cases/find-all-posts.use-case';
import { FindPostByIdUseCase } from './application/use-cases/find-post-by-id.use-case';
import { DeleteCommentPostUseCase } from './application/use-cases/remove-comment.use-case';
import { UpdateCommentPostUseCase } from './application/use-cases/update-comment-post.use-case';
import { UpdatePostUseCase } from './application/use-cases/update-post.use-case';
import { PostService } from './domain/post.service';
import { PostController } from './infrastructure/post.controller';
import { PostRepositoryMongo } from './infrastructure/post.repository.mongo';
import { PostMongo, PostSchema } from './infrastructure/post.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: PostMongo.name, schema: PostSchema }]),
    OffensiveWordModule,
  ],
  providers: [
    PostService,
    PostRepositoryMongo,
    CreatePostUseCase,
    DeletePostUseCase,
    FindAllPostsUseCase,
    FindPostByIdUseCase,
    UpdatePostUseCase,
    AddCommentToPostUseCase,
    UpdateCommentPostUseCase,
    DeleteCommentPostUseCase,
    CheckIfPostIdExistUseCase,
    CheckIfCommentIdExistUseCase
  ],
  controllers: [PostController],
  
})
export class PostsModule {}
