/* eslint-disable prettier/prettier */
import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  Logger,
  PipeTransform
} from '@nestjs/common';
import { CheckIfPostIdExistUseCase } from '../../application/use-cases/check-if-post-exist.use-case';

@Injectable()
export class CheckIfPostIdExistPipe implements PipeTransform {
  constructor(private checkExistPostUseCase: CheckIfPostIdExistUseCase) {}

  async transform(value: any, metadata: ArgumentMetadata): Promise<string> {
    Logger.debug(`CheckIfPostIdExistPipe value: ${value}`);
    const exists = await this.checkExistPostUseCase.execute(value);
    if (!exists) {
      throw new BadRequestException(
        `Post with ID ${JSON.stringify(
          value,
        )} no exists. Metadata: ${JSON.stringify(metadata)}}`,
      );
    }
    return value;
  }
}
