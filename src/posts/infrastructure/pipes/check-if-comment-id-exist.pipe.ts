/* eslint-disable prettier/prettier */
import {
    ArgumentMetadata,
    BadRequestException,
    Injectable, PipeTransform
} from '@nestjs/common';
import { CheckIfCommentIdExistUseCase } from '../../application/use-cases/check-if-comment-exist.use-case';

@Injectable()
export class CheckIfCommentIdExistPipe implements PipeTransform {
  constructor(private checkIfCommentExistUseCase: CheckIfCommentIdExistUseCase) {}

  async transform(value: any, metadata: ArgumentMetadata): Promise<string> {
    const exists =
      await this.checkIfCommentExistUseCase.execute(
        value.postId,
        value.commentId,
      );
    if (!exists) {
      throw new BadRequestException(
        `Post with ID ${JSON.stringify(
          value,
        )} no exists. Metadata: ${JSON.stringify(metadata)}}`,
      );
    }
    return value;
  }
}
