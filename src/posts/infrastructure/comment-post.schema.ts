import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type CommentPostDocument = CommentPostMongo & Document;

@Schema()
export class CommentPostMongo {
  @Prop({
    type: String,
    required: true,
  })
  id: string;

  @Prop({
    type: String,
    required: true,
  })
  nickname: string;

  @Prop({
    type: String,
    required: true,
  })
  content: string;

  @Prop({
    type: String,
    required: true,
  })
  date: string;
}

export const CommentPostSchema = SchemaFactory.createForClass(CommentPostMongo);
