import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { AnyObject, Model } from 'mongoose';
import { AuthorNameVO } from '../../authors/domain/vo/author-name.vo';
import { AuthorNicknameVO } from '../../authors/domain/vo/author-nickname.vo';
import { IdVO } from '../../common/domain/vo/id.vo';
import { CommentPost } from '../domain/entity/comment-post.entity';
import { Post, PostType } from '../domain/entity/post.entity';
import { PostRepository } from '../domain/post.repository';
import { CommentContentVO } from '../domain/vo/comment-content.vo';
import { CommentDateVO } from '../domain/vo/comment-date.vo';
import { CommentNicknameVO } from '../domain/vo/comment-nickname.vo';
import { CommentsListVO } from '../domain/vo/comments-list.vo';
import { PostContentVO } from '../domain/vo/post-content.vo';
import { PostTitleVO } from '../domain/vo/post-title.vo';
import { PostDocument, PostMongo as PostModel } from './post.schema';

@Injectable()
export class PostRepositoryMongo implements PostRepository {
  constructor(
    @InjectModel(PostModel.name) private postModel: Model<PostDocument>,
  ) {}

  async getAllPosts(): Promise<Post[]> {
    const allPostsData = await this.postModel.find().exec();
    return allPostsData.map((post: AnyObject) => {
      const postToType: PostType = this.castPostSchemaToType(post);
      return new Post(postToType);
    });
  }

  async getPostByID(postID: IdVO): Promise<Post> {
    const searchedPost: AnyObject = await this.getPostModel(postID);
    const postType: PostType = this.castPostSchemaToType(searchedPost);
    return new Post(postType);
  }

  async persistPost(post: Post): Promise<void> {
    const newPost: AnyObject = post.toAnyType();
    const postModel = new this.postModel(newPost);
    await postModel.save();
  }

  async updatePost(postID: IdVO, post: Post): Promise<Post> {
    const { id, author, nickname, title, content }: AnyObject =
      post.toAnyType();
    const updatedPost = { id, author, nickname, title, content };
    const returnedPost: AnyObject = await this.postModel.findOneAndUpdate(
      { id: postID.value },
      updatedPost,
    );
    const returnedPostToType: PostType =
      this.castPostSchemaToType(returnedPost);
    return new Post(returnedPostToType);
  }

  async deletePostByID(postID: IdVO): Promise<void> {
    await this.postModel.deleteOne({ id: postID.value }).exec();
  }

  async saveCommentInPost(postID: IdVO, comment: CommentPost): Promise<void> {
    const searchedPost: AnyObject = await this.getPostModel(postID);
    const newComment = {
      id: comment.id.value,
      nickname: comment.nickname.value,
      content: comment.content.value,
      date: comment.date.value,
    };
    await searchedPost.comments.push(newComment);
    await searchedPost.save();
  }

  async getCommentPostByID(
    postID: IdVO,
    commentID: IdVO,
  ): Promise<CommentPost> {
    const comment: AnyObject = this.postModel
      .findOne({
        id: postID.value,
        'comments.id': commentID.value,
      })
      .exec();

    return new CommentPost({
      id: IdVO.createWithUUID(comment.id),
      nickname: CommentNicknameVO.create(comment.nickname),
      content: CommentContentVO.create(comment.content),
      date: CommentDateVO.createWithDate(comment.date),
    });
  }

  async updateCommentInPost(
    postID: IdVO,
    updatedComment: CommentPost,
  ): Promise<void> {
    const query = { id: postID.value, 'comments.id': updatedComment.id.value };
    const updatedDocument = {
      $set: {
        'comments.$.nickname': updatedComment.nickname.value,
        'comments.$.content': updatedComment.content.value,
        'comments.$.date': updatedComment.date.value,
      },
    };
    await this.postModel.updateOne(query, updatedDocument);
  }

  async deleteCommentInPost(postID: IdVO, commentID: IdVO): Promise<void> {
    await this.postModel.updateOne(
      { id: postID.value },
      {
        $pull: {
          comments: { id: commentID.value },
        },
      },
    );
  }

  async checkIfPostExists(id: IdVO): Promise<boolean> {
    return this.postModel.exists({ id: id.value });
  }

  async checkIfCommentPostExists(
    postID: IdVO,
    commentID: IdVO,
  ): Promise<boolean> {
    return this.postModel.exists({
      id: postID.value,
      'comments.id': commentID.value,
    });
  }

  async deleteAll(): Promise<void> {
    await this.postModel.deleteMany();
  }

  // Class utils
  private async getPostModel(postID: IdVO): Promise<AnyObject> {
    return this.postModel.findOne({ id: postID.value }).exec();
  }

  private castPostSchemaToType(post: AnyObject): PostType {
    return {
      id: IdVO.createWithUUID(post.id),
      author: AuthorNameVO.create(post.author),
      nickname: AuthorNicknameVO.create(post.nickname),
      title: PostTitleVO.create(post.title),
      content: PostContentVO.create(post.content),
      comments: CommentsListVO.create(
        post.comments.map((c: AnyObject) => {
          return new CommentPost({
            id: IdVO.createWithUUID(c.id),
            nickname: CommentNicknameVO.create(c.nickname),
            content: CommentContentVO.create(c.content),
            date: CommentDateVO.createWithDate(c.date),
          });
        }),
      ),
    };
  }
}
