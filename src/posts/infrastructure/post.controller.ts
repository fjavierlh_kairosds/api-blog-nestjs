/* eslint-disable prettier/prettier */
import {
  Body,
  Controller,
  Delete,
  Get, Param,
  Post,
  Put,
  Res,
  UseGuards
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth } from '@nestjs/swagger';
import { Response } from 'express';
import { CreateCommentDTO } from '../application/dto/create-comment.dto';
import { CreatePostDTO } from '../application/dto/create-post.dto';
import { UpdateCommentDTO } from '../application/dto/update-comment.dto';
import { UpdatePostDTO } from '../application/dto/update-post.dto';
import { AddCommentToPostUseCase } from '../application/use-cases/add-comment-to-post.use-case';
import { CreatePostUseCase } from '../application/use-cases/create-post.use-case';
import { DeletePostUseCase } from '../application/use-cases/delete-post.use-case';
import { FindAllPostsUseCase } from '../application/use-cases/find-all-posts.use-case';
import { FindPostByIdUseCase } from '../application/use-cases/find-post-by-id.use-case';
import { DeleteCommentPostUseCase } from '../application/use-cases/remove-comment.use-case';
import { UpdateCommentPostUseCase } from '../application/use-cases/update-comment-post.use-case';
import { UpdatePostUseCase } from '../application/use-cases/update-post.use-case';
import { CheckIfCommentIdExistPipe } from './pipes/check-if-comment-id-exist.pipe';
import { CheckIfPostIdExistPipe } from './pipes/check-if-post-id-exist.pipe';

@Controller()
export class PostController {
  constructor(
    private createPostUseCase: CreatePostUseCase,
    private deletePostUseCase: DeletePostUseCase,
    private findAllPostsUseCase: FindAllPostsUseCase,
    private findPostByIdUseCase: FindPostByIdUseCase,
    private updatePostUseCase: UpdatePostUseCase,
    private addCommentToPostUseCase: AddCommentToPostUseCase,
    private updateCommentUseCase: UpdateCommentPostUseCase,
    private deleteCommentUseCase: DeleteCommentPostUseCase,
  ) {}

  @Post('posts')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  async publishPost(@Body() post: CreatePostDTO, @Res() res: Response) {
    try {
      const { author, nickname, title, content } = post;
      const createPostDTO: CreatePostDTO = { author, nickname, title, content };
      const newPost = await this.createPostUseCase.execute(createPostDTO);
      res.status(201).json({
        statusCode: 201,
        message: `Save post with ID '${newPost.id}' success`,
        newPost,
      });
    } catch (error) {
      res.status(401).json({ errors: { msg: error.message } });
    }
  }

  @Delete('posts/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  async deletePost(
    @Param('id', CheckIfPostIdExistPipe) id: string,
    @Res() res: Response,
  ) {
    try {
      await this.deletePostUseCase.execute(id);
      return res.status(200).json({
        statusCode: 200,
        message: `Deleted post with ID '${id}' success`,
      });
    } catch (error) {
      res.status(401).json({ errors: { msg: error.message } });
    }
  }

  @Get('posts')
  async getAllPosts(@Res() res: Response) {
    try {
      const allPosts = await this.findAllPostsUseCase.execute();
      res.status(200).json({
        statusCode: 200,
        message: `Showing ${allPosts.length} posts`,
        allPosts,
      });
    } catch (error) {
      res.status(401).json({ errors: { msg: error.message } });
    }
  }

  @Get('posts/:id')
  async getPostByID(
    @Param('id', CheckIfPostIdExistPipe) id: string,
    @Res() res: Response,
  ) {
    try {
      const searchedPost = await this.findPostByIdUseCase.execute(id);
      return res.status(200).json({
        statusCode: 200,
        message: `Post with ID ${id} found`,
        searchedPost,
      });
    } catch (error) {
      res.status(401).json({ errors: { msg: error.message } });
    }
  }

  @Put('posts/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  async updatePost(
    @Param('id', CheckIfPostIdExistPipe) id: string,
    @Body() updatedPost: UpdatePostDTO,
    @Res() res: Response,
  ) {
    try {
      await this.updatePostUseCase.execute(id, updatedPost);
      return res.status(200).json({
        statusCode: 200,
        message: `Updated post with ID '${id}' success`,
        updatedPost,
      });
    } catch (error) {
      res.status(401).json({ errors: { msg: error.message } });
    }
  }

  @Post(':postId/comments')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  async addComment(
    @Param('postId', CheckIfPostIdExistPipe) postId: string,
    @Body() comment: CreateCommentDTO,
    @Res() res: Response,
  ) {
    try {
      const newComment = await this.addCommentToPostUseCase.execute(
        postId,
        comment,
      );
      res.status(200).json({
        statusCode: 200,
        message: `Save comment with ID '${newComment.id}' on post '${postId}' success`,
        newComment,
      });
    } catch (error) {
      res.status(401).json({ errors: { msg: error.message } });
    }
  }

  @Put(':postId/comments/:commentId')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  async updateCommentPost(
    @Param(CheckIfCommentIdExistPipe) params: any,
    @Body() updateComment: UpdateCommentDTO,
    @Res() res: Response,
  ) {
    try {
      const { postId, commentId } = params;
      const updatedComment = await this.updateCommentUseCase.execute(
        postId,
        commentId,
        updateComment,
      );
      res.status(200).json({
        statusCode: 200,
        message: `Update comment with ID '${commentId}' on post '${postId}' success`,
        updatedComment,
      });
    } catch (error) {
      res.status(401).json({ errors: { msg: error.message } });
    }
  }

  @Delete(':postId/comments/:commentId')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  async deleteComment(
    @Param('postId') postId: string,
    @Param('commentId') commentId: string,
    @Res() res: Response,
  ) {
    try {
      await this.deleteCommentUseCase.execute(postId, commentId);
      res.status(200).json({
        statusCode: 200,
        message: `Delete comment with ID '${commentId}' on post '${postId}' success`,
      });
    } catch (error) {
      res.status(401).json({ errors: { msg: error.message } });
    }
  }
}
