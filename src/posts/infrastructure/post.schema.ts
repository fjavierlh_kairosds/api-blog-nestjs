import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { CommentPostDocument, CommentPostSchema } from './comment-post.schema';

export type PostDocument = PostMongo & Document;

@Schema({ collection: 'Posts' })
export class PostMongo {
  @Prop({
    type: String,
    required: true,
  })
  id: string;

  @Prop({
    type: String,
    required: true,
  })
  author: string;

  @Prop({
    type: String,
    required: true,
  })
  nickname: string;

  @Prop({
    type: String,
    required: true,
  })
  title: string;

  @Prop({
    type: String,
    required: true,
  })
  content: string;

  @Prop({
    type: [CommentPostSchema],
    required: true,
  })
  comments: Array<CommentPostDocument>;
}

export const PostSchema = SchemaFactory.createForClass(PostMongo);
