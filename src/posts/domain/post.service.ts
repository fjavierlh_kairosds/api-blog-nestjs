import { Injectable } from '@nestjs/common';
import { IdVO } from '../../common/domain/vo/id.vo';
import { PostRepositoryMongo } from '../infrastructure/post.repository.mongo';
import { CommentPost } from './entity/comment-post.entity';
import { Post } from './entity/post.entity';

@Injectable()
export class PostService {
  constructor(private postRepository: PostRepositoryMongo) {}

  async findAllPosts(): Promise<Post[]> {
    return this.postRepository.getAllPosts();
  }

  async findPostById(postID: IdVO): Promise<Post> {
    await this.checkIfPostExist(postID);
    return this.postRepository.getPostByID(postID);
  }

  async savePost(newPost: Post): Promise<void> {
    await this.postRepository.persistPost(newPost);
  }

  async updatePostByID(idPost: IdVO, post: Post): Promise<Post> {
    await this.checkIfPostExist(idPost);
    return this.postRepository.updatePost(idPost, post);
  }

  async removePostById(postID: IdVO): Promise<void> {
    await this.checkIfPostExist(postID);
    await this.postRepository.deletePostByID(postID);
  }

  async commentPost(postID: IdVO, newComment: CommentPost): Promise<void> {
    await this.checkIfPostExist(postID);
    await this.postRepository.saveCommentInPost(postID, newComment);
  }

  async updateCommentPost(
    postID: IdVO,
    updatedComment: CommentPost,
  ): Promise<void> {
    await this.checkIfCommentPostExist(postID, updatedComment.id);
    await this.postRepository.updateCommentInPost(postID, updatedComment);
  }

  async removeCommentPost(postID: IdVO, commentID: IdVO): Promise<void> {
    await this.checkIfCommentPostExist(postID, commentID);
    await this.postRepository.deleteCommentInPost(postID, commentID);
  }

  async checkIfPostExist(postID: IdVO): Promise<boolean> {
    return this.postRepository.checkIfPostExists(postID);
  }

  async checkIfCommentPostExist(
    postID: IdVO,
    commentID: IdVO,
  ): Promise<boolean> {
    await this.checkIfPostExist(postID);
    return this.postRepository.checkIfCommentPostExists(postID, commentID);
  }
}
