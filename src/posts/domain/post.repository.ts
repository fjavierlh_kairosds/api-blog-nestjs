import { IdVO } from '../../common/domain/vo/id.vo';
import { CommentPost } from './entity/comment-post.entity';
import { Post } from './entity/post.entity';

export interface PostRepository {
  getAllPosts(): Promise<Post[]>;

  getPostByID(postID: IdVO): Promise<Post>;

  persistPost(post: Post): Promise<void>;

  updatePost(postID: IdVO, updatedPost: Post): Promise<Post>;

  deletePostByID(postID: IdVO): Promise<void>;

  saveCommentInPost(postID: IdVO, comment: CommentPost): Promise<void>;

  updateCommentInPost(postID: IdVO, updatedComment: CommentPost): Promise<void>;

  getCommentPostByID(postID: IdVO, commentID: IdVO): Promise<CommentPost>;

  deleteCommentInPost(postID: IdVO, commentID: IdVO): Promise<void>;

  checkIfPostExists(postID: IdVO): Promise<boolean>;

  checkIfCommentPostExists(postID: IdVO, commentID: IdVO): Promise<boolean>;

  deleteAll(): Promise<void>;
}
