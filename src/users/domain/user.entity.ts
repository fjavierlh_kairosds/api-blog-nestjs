import { IdVO } from '../../common/domain/vo/id.vo';
import { EmailVO } from './vo/email.vo';
import { PasswordVO } from './vo/password.vo';
import { RoleVO } from './vo/role.vo';

export type UserType = {
  id: IdVO;
  email: EmailVO;
  password: PasswordVO;
  role: RoleVO;
};

export class User {
  constructor(private user: UserType) {}

  get id(): IdVO {
    return this.user.id;
  }

  get email(): EmailVO {
    return this.user.email;
  }

  get password(): PasswordVO {
    return this.user.password;
  }

  get role(): RoleVO {
    return this.user.role;
  }
}
