import { IdVO } from '../../common/domain/vo/id.vo';
import { User, UserType } from './user.entity';
import { EmailVO } from './vo/email.vo';
import { PasswordVO } from './vo/password.vo';
import { Role, RoleVO } from './vo/role.vo';
describe('User test suite', () => {
  it('should create a user', () => {
    const validUUID = '3b37664b-7292-48ba-a986-55480402d683';
    const validEmail = 'hi@mymail.com';
    const validPassword = '@Bcd3fgh1#';
    const validRole = 'PUBLISHER';

    const userData: UserType = {
      id: IdVO.createWithUUID(validUUID),
      email: EmailVO.create(validEmail),
      password: PasswordVO.create(validPassword),
      role: RoleVO.create(Role.PUBLISHER),
    };

    const expectedUserEntity = new User(userData);

    expect(expectedUserEntity.id.value).toBe(validUUID);
    expect(expectedUserEntity.email.value).toBe(validEmail);
    expect(expectedUserEntity.password.value).toBe(validPassword);
    expect(expectedUserEntity.role.value).toBe(validRole);
  });
});
