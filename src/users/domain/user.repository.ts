import { User } from './user.entity';
import { EmailVO } from './vo/email.vo';

export interface UsersRepository {
  saveUser(user: User): Promise<void>;

  getUserByEmail(email: EmailVO): Promise<User | null>;

  updateUserByEmail(email: EmailVO, updatedUser: User): Promise<void | null>;

  deleteUser(email: EmailVO): Promise<void | null>;

  deleteAll(): Promise<void>;
}
