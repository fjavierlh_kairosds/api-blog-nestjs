import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { ExceptionWithCode } from '../../common/domain/custom-errors/exception-with-code';
import { parseEnvVariableToNumber } from '../../common/utils/parse-env-variable-to-number.util';
import { UsersRepositoryPostgres } from '../infrastructure/user.repository.pg';
import { User, UserType } from './user.entity';
import { EmailVO } from './vo/email.vo';
import { PasswordVO } from './vo/password.vo';

@Injectable()
export class UserService {
  constructor(private userRepository: UsersRepositoryPostgres) {}

  async isValidPassword(password: PasswordVO, user: User): Promise<boolean> {
    return bcrypt.compare(password.value, user.password.value);
  }

  async persist(user: User): Promise<void> {
    const hash = await bcrypt.hash(
      user.password.value,
      parseEnvVariableToNumber(process.env.SALT ?? '10'),
    );

    const encryptedPassword = PasswordVO.create(hash);

    const newUser: UserType = {
      id: user.id,
      email: user.email,
      password: encryptedPassword,
      role: user.role,
    };

    await this.userRepository.saveUser(new User(newUser));
  }

  async findByEmail(email: EmailVO): Promise<User | null> {
    return this.userRepository.getUserByEmail(email);
  }

  async updateByEmail(
    email: EmailVO,
    updatedUser: UserType,
  ): Promise<void | null> {
    await this.checkIfEmailExist(email);
    await this.userRepository.updateUserByEmail(email, new User(updatedUser));
  }

  async deleteUser(email: EmailVO): Promise<void | null> {
    await this.userRepository.deleteUser(email);
  }

  private async checkIfEmailExist(email: EmailVO): Promise<void> {
    const user = await this.userRepository.getUserByEmail(email);
    if (!user)
      throw new ExceptionWithCode(
        404,
        `Doens't exist user with email ${email.value}`,
      );
  }
}
