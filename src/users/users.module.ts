import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { SequelizeModule } from '@nestjs/sequelize';
import { AuthorsModule } from '../authors/authors.module';
import { EnvConfig } from '../common/infrastructure/config/env.config';
import { JwtRegisterModule } from '../common/infrastructure/config/jwt.module';
import { JwtStrategy } from '../common/infrastructure/config/jwt.strategy';
import { SignInUserUseCase } from './application/use-cases/sign-in-user.use-case';
import { SignUpUserUseCase } from './application/use-cases/sign-up-user.use-case';
import { UserService } from './domain/user.service';
import { UserController } from './infrastructure/user.controller';
import { UserModel } from './infrastructure/user.model';
import { UsersRepositoryPostgres } from './infrastructure/user.repository.pg';

@Module({
  imports: [
    EnvConfig,
    SequelizeModule.forFeature([UserModel]),
    AuthorsModule,
    PassportModule,
    JwtRegisterModule,
  ],
  providers: [
    SignUpUserUseCase,
    SignInUserUseCase,
    UserService,
    UsersRepositoryPostgres,
    JwtStrategy,
  ],
  controllers: [UserController],
  exports: [
    UsersModule,
    SignUpUserUseCase,
    SequelizeModule.forFeature([UserModel]),
    SignInUserUseCase,
    PassportModule,
    UserService,
    UsersRepositoryPostgres,
  ],
})
export class UsersModule {}
