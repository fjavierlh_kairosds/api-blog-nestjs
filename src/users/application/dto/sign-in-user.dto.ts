import { ApiProperty } from '@nestjs/swagger';

export class SignInUserDTO {
  @ApiProperty()
  readonly email: string;

  @ApiProperty()
  readonly password: string;

  constructor(email: string, password: string) {
    this.email = email;
    this.password = password;
  }
}
