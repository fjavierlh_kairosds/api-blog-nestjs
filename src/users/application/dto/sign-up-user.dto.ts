import { ApiProperty } from '@nestjs/swagger';

export class SignUpUserDTO {
  @ApiProperty()
  readonly email: string;

  @ApiProperty()
  readonly password: string;

  @ApiProperty()
  readonly name: string;

  @ApiProperty()
  readonly nickname: string;

  constructor(email: string, password: string, name: string, nickname: string) {
    this.email = email;
    this.password = password;
    this.name = name;
    this.nickname = nickname;
  }
}
