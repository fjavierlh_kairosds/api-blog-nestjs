import { SignUpUserUseCase } from './sign-up-user.use-case';

export const UserUseCases = [SignUpUserUseCase];
