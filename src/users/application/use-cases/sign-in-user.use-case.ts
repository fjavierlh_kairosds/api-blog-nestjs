import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from 'jsonwebtoken';
import { ExceptionWithCode } from '../../../common/domain/custom-errors/exception-with-code';
import { User } from '../../domain/user.entity';
import { UserService } from '../../domain/user.service';
import { EmailVO } from '../../domain/vo/email.vo';
import { PasswordVO } from '../../domain/vo/password.vo';
import { SignInUserDTO } from '../dto/sign-in-user.dto';

@Injectable()
export class SignInUserUseCase {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  async execute(request: SignInUserDTO): Promise<string | null> {
    const user: User | null = await this.userService.findByEmail(
      EmailVO.create(request.email),
    );
    if (!user) throw new ExceptionWithCode(404, 'User not found');

    const plainPass = PasswordVO.create(request.password);
    const isValid = await this.userService.isValidPassword(plainPass, user);

    if (isValid) {
      const payload: JwtPayload = {
        id: user.id.value,
        email: user.email.value,
      };
      return this.jwtService.sign(payload);
    }

    return null;
  }
}
