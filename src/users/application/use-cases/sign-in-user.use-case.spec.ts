import { Test } from '@nestjs/testing';
import 'reflect-metadata';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { EnvConfig } from '../../../common/infrastructure/config/env.config';
import { JwtRegisterModule } from '../../../common/infrastructure/config/jwt.module';
import { JwtStrategy } from '../../../common/infrastructure/config/jwt.strategy';
import { User } from '../../domain/user.entity';
import { UserService } from '../../domain/user.service';
import { EmailVO } from '../../domain/vo/email.vo';
import { PasswordVO } from '../../domain/vo/password.vo';
import { Role, RoleVO } from '../../domain/vo/role.vo';
import { UsersRepositoryPostgres } from '../../infrastructure/user.repository.pg';
import { SignInUserUseCase } from './sign-in-user.use-case';
import { SignInRequest } from './types/sign-in.request';

jest.mock('../../infrastructure/user.repository.pg', () => {
  return {
    UsersRepositoryPostgres: jest.fn().mockImplementation(() => {
      return {
        getUserByEmail: jest.fn().mockImplementation(() => {
          return new User({
            id: IdVO.createWithUUID('f2dd593e-af8e-4754-bed3-b42d2cfce636'),
            email: EmailVO.create('hi@mymail.com'),
            password: PasswordVO.create(
              '$2b$10$Evl2ukNLSMrqpUB4vjZNCOEkAG3LAdXzOwWhHKtw.KRvW5Grn0NlW',
            ),
            role: RoleVO.create(Role.PUBLISHER),
          });
        }),
      };
    }),
  };
});

describe('SignInUserUseCase test suite', () => {
  let usersRepository: UsersRepositoryPostgres;
  let signInUserUseCase: SignInUserUseCase;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [EnvConfig, JwtRegisterModule],
      providers: [
        SignInUserUseCase,
        UserService,
        UsersRepositoryPostgres,
        JwtStrategy,
      ],
    }).compile();

    usersRepository = moduleRef.get<UsersRepositoryPostgres>(
      UsersRepositoryPostgres,
    );

    signInUserUseCase = moduleRef.get<SignInUserUseCase>(SignInUserUseCase);
  });

  it('should sign in an user with valid credentials', async () => {
    const spyRepository = jest.spyOn(usersRepository, 'getUserByEmail');
    const userTest: SignInRequest = {
      email: 'hi@mymail.com',
      password: '@bCd3FgH1!',
    };

    const returnedToken = await signInUserUseCase.execute(userTest);
    expect(spyRepository).toHaveBeenCalled();
    expect(typeof returnedToken).toBe('string');
  });

  it('should return null if enter invalid credentials', async () => {
    const userTest: SignInRequest = {
      email: 'hi@mymail.com',
      password: '12345',
    };

    const returnedToken = await signInUserUseCase.execute(userTest);
    expect(returnedToken).toBeNull();
  });
});
