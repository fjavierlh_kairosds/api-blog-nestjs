import { Test } from '@nestjs/testing';
import 'reflect-metadata';
import { CreateAuthorUseCase } from '../../../authors/application/use-cases/create-author.use-case';
import { AuthorsService } from '../../../authors/domain/authors.service';
import { AuthorsRepositoryMongo } from '../../../authors/infrastructure/author.repository.mongo';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { UserService } from '../../domain/user.service';
import { UsersRepositoryPostgres } from '../../infrastructure/user.repository.pg';
import { SignUpUserDTO } from '../dto/sign-up-user.dto';
import { SignUpUserUseCase } from './sign-up-user.use-case';

jest.mock('../../infrastructure/user.repository.pg', () => {
  return {
    UsersRepositoryPostgres: jest.fn().mockImplementation(() => {
      return {
        saveUser: jest.fn(),
      };
    }),
  };
});
jest.mock('../../../authors/infrastructure/author.repository.mongo', () => {
  return {
    AuthorsRepositoryMongo: jest.fn().mockImplementation(() => {
      return {
        createAuthor: jest.fn(),
      };
    }),
  };
});

describe('SignUpUserUseCase test suite', () => {
  let usersRepository: UsersRepositoryPostgres;
  let auhtorsRepository: AuthorsRepositoryMongo;
  let signUpUserUseCase: SignUpUserUseCase;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        SignUpUserUseCase,
        UserService,
        UsersRepositoryPostgres,
        CreateAuthorUseCase,
        AuthorsService,
        AuthorsRepositoryMongo,
      ],
    }).compile();

    usersRepository = moduleRef.get<UsersRepositoryPostgres>(
      UsersRepositoryPostgres,
    );

    auhtorsRepository = moduleRef.get<AuthorsRepositoryMongo>(
      AuthorsRepositoryMongo,
    );

    signUpUserUseCase = moduleRef.get<SignUpUserUseCase>(SignUpUserUseCase);
  });

  it('should sign up an user and return a valid UUID', async () => {
    const userTest: SignUpUserDTO = {
      email: 'hi@mymail.com',
      password: '@bCd3fGh1#',
      name: 'testuser',
      nickname: 'nicktest',
    };

    const receivedUUID = await signUpUserUseCase.execute(userTest);
    expect(usersRepository.saveUser).toHaveBeenCalled();
    expect(auhtorsRepository.createAuthor).toHaveBeenCalled();
    expect(() => IdVO.createWithUUID(receivedUUID)).not.toThrow();
  });
});
