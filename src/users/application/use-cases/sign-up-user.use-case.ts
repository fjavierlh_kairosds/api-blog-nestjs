import { Injectable } from '@nestjs/common';
import { CreateAuthorUseCase } from '../../../authors/application/use-cases/create-author.use-case';
import { IdVO } from '../../../common/domain/vo/id.vo';
import { User, UserType } from '../../domain/user.entity';
import { UserService } from '../../domain/user.service';
import { EmailVO } from '../../domain/vo/email.vo';
import { PasswordVO } from '../../domain/vo/password.vo';
import { Role, RoleVO } from '../../domain/vo/role.vo';
import { SignUpUserDTO } from '../dto/sign-up-user.dto';

@Injectable()
export class SignUpUserUseCase {
  constructor(
    private userService: UserService,
    private createAuthorUseCase: CreateAuthorUseCase,
  ) {}

  async execute(request: SignUpUserDTO): Promise<string> {
    const { email, password, name, nickname } = request;

    const user: UserType = {
      id: IdVO.create(),
      email: EmailVO.create(email),
      password: PasswordVO.create(password),
      role: RoleVO.create(Role.PUBLISHER),
    };

    await this.userService.persist(new User(user));

    const id = user.id.value;
    await this.createAuthorUseCase.execute(id, { name, nickname });

    return id;
  }
}
