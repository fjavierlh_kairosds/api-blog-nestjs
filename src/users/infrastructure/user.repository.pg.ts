import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { IdVO } from '../../common/domain/vo/id.vo';
import { User, UserType } from '../domain/user.entity';
import { UsersRepository } from '../domain/user.repository';
import { EmailVO } from '../domain/vo/email.vo';
import { PasswordVO } from '../domain/vo/password.vo';
import { RoleVO } from '../domain/vo/role.vo';
import { UserModel } from './user.model';

@Injectable()
export class UsersRepositoryPostgres implements UsersRepository {
  constructor(@InjectModel(UserModel) private userModel: typeof UserModel) {}

  async deleteUser(email: EmailVO): Promise<void | null> {
    const rowsDeleted = await this.userModel.destroy({
      where: {
        email: email.value,
      },
    });

    if (!rowsDeleted) return null;
  }

  async updateUserByEmail(
    email: EmailVO,
    updatedUser: User,
  ): Promise<void | null> {
    const user: User | null = await this.getUserByEmail(email);

    if (!user) return null;

    const id = user.id.value;
    const updatedEmail = updatedUser.email.value;
    const updatedPassword = updatedUser.password.value;

    await this.userModel.update(
      {
        email: updatedEmail,
        password: updatedPassword,
      },
      {
        where: {
          id: id,
        },
      },
    );
  }

  async saveUser(user: User): Promise<void> {
    const id = user.id.value;
    const email = user.email.value;
    const password = user.password.value;
    const role = user.role.value;

    const userModel = this.userModel.build({ id, email, password, role });
    await userModel.save();
  }

  async getUserByEmail(email: EmailVO): Promise<User | null> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const user: any | null = await this.userModel.findOne({
      where: { email: email.value },
    });

    if (!user) return null;

    const userData: UserType = {
      id: IdVO.createWithUUID(user.id),
      email: EmailVO.create(user.email),
      password: PasswordVO.create(user.password),
      role: RoleVO.create(user.role),
    };

    return new User(userData);
  }

  checkIfUserExist(value: any): Promise<boolean> {
    throw this.userModel.findOne(value);
  }

  async deleteAll(): Promise<void> {
    await this.userModel.destroy({ where: {}, truncate: true });
  }
}
