/* eslint-disable prettier/prettier */
import {
    ArgumentMetadata,
    BadRequestException,
    Injectable,
    Logger,
    PipeTransform
} from '@nestjs/common';
import { UsersRepositoryPostgres } from './user.repository.pg';

@Injectable()
export class ValidUserPipe implements PipeTransform {
  constructor(private usersService: UsersRepositoryPostgres) {}

  async transform(value: any, metadata: ArgumentMetadata): Promise<string> {
    try {
      Logger.debug(`ValidUserPipe: - value: ${value}`);
      await this.usersService.checkIfUserExist(value);
    } catch (error: any) {
      throw new BadRequestException(
        `User ID ${JSON.stringify(value)} no exists. Metadata: ${JSON.stringify(
          metadata,
        )}}`,
      );
    }
    return value;
  }
}
