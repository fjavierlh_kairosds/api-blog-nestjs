/* eslint-disable prettier/prettier */
import {
  Body,
  Controller,
  Post,
  Res,
  UnauthorizedException
} from '@nestjs/common';
import { Response } from 'express';
import { SignInUserDTO } from '../application/dto/sign-in-user.dto';
import { SignUpUserDTO } from '../application/dto/sign-up-user.dto';
import { SignInUserUseCase } from '../application/use-cases/sign-in-user.use-case';
import { SignUpUserUseCase } from '../application/use-cases/sign-up-user.use-case';

@Controller('users')
export class UserController {
  constructor(
    private signUpUseCase: SignUpUserUseCase,
    private signInUseCase: SignInUserUseCase,
  ) {}

  @Post('sign-up')
  async signUp(
    @Body() user: SignUpUserDTO,
    @Res() res: Response,
  ): Promise<void> {
    try {
      const userId = await this.signUpUseCase.execute(user);
      res.status(201).json({ status: `Created user with id '${userId}'` });
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  }

  @Post('sign-in')
  async signIn(@Body() user: SignInUserDTO, @Res() res: Response) {
    try {
      const { email, password } = user;
      const loginData: SignInUserDTO = { email, password };
      const token = await this.signInUseCase.execute(loginData);
      if (!token) throw new UnauthorizedException();
      res.status(201).json({ token: token });
    } catch (error) {
      res.status(401).json({ error: error.message });
    }
  }
}
