import { DataTypes } from 'sequelize';
import { Column, Model, Table } from 'sequelize-typescript';
import { Role } from '../domain/vo/role.vo';
//import sequelize from '../config/postgres';

@Table({ tableName: 'users' })
export class UserModel extends Model {
  @Column({
    type: DataTypes.UUID,
    allowNull: false,
    primaryKey: true,
  })
  id: string;

  @Column({
    type: DataTypes.STRING,
    allowNull: false,
    unique: true,
  })
  email: string;

  @Column({
    type: DataTypes.STRING,
    allowNull: false,
  })
  password: string;

  @Column({
    type: DataTypes.ENUM('ADMIN', 'PUBLISHER'),
    allowNull: false,
  })
  role: Role;
}
