import { MongooseModule } from '@nestjs/mongoose';

export const MongooseConfig = MongooseModule.forRoot(
  `mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB_NAME}`,
  {
    authSource: process.env.MONGO_AUTH_SOURCE,
    auth: {
      user: process.env.MONGO_DB_USER,
      password: process.env.MONGO_DB_PASSWORD,
    },
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false,
  },
);
