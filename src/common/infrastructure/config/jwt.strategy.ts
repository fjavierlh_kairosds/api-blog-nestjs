import { Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { JwtPayload } from 'jsonwebtoken';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { User } from '../../../users/domain/user.entity';
import { UserService } from '../../../users/domain/user.service';
import { EmailVO } from '../../../users/domain/vo/email.vo';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private userService: UserService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.AUTH_TOKEN_SECRET,
    });
  }

  async validate(payload: JwtPayload): Promise<User> {
    const user = await this.userService.findByEmail(
      EmailVO.create(payload.email),
    );
    Logger.debug(user);
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
