import { JwtModule } from '@nestjs/jwt';

export const JwtRegisterModule = JwtModule.register({
  secret: process.env.AUTH_TOKEN_SECRET,
  signOptions: { expiresIn: process.env.TOKEN_EXPIRATION },
});
