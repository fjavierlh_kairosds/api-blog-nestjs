import { SequelizeModule } from '@nestjs/sequelize';
import { UserModel } from '../../../users/infrastructure/user.model';

export const SequelizeConfig = SequelizeModule.forRoot({
  dialect: 'postgres',
  host: process.env.POSTGRES_HOST,
  port: +process.env.POSTGRES_PORT,
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DB,
  models: [UserModel],
  autoLoadModels: true,
  synchronize: true,
  logging: false,
});
