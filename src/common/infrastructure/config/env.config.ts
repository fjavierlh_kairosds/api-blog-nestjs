import { ConfigModule } from '@nestjs/config';

const ENV = process.env.ENVIRON;

export const EnvConfig = ConfigModule.forRoot({
  envFilePath: !ENV || ENV === 'dev' ? '.env' : `.env.${ENV}`,
});
