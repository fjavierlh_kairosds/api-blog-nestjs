import { SetMetadata } from '@nestjs/common';
import { Role } from '../../users/domain/vo/role.vo';

export const Roles = (...roles: Role[]) => SetMetadata('roles', roles);
