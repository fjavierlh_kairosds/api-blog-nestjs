import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { UserService } from '../../users/domain/user.service';
import { Role } from '../../users/domain/vo/role.vo';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private userService: UserService, private reflector: Reflector) {}
  canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.get<Role[]>('roles', context.getHandler());
    const [req] = context.getArgs();
    const userRole = req.user.role.value;
    return roles.some((role) => userRole === role);
  }
}
